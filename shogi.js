// ------------------------------
// 将棋。
// インタフェースに近い動作を定義。
// 
// ------------------------------


// ------------------------------
// 定数
// ------------------------------
// 無し
var NONSTR = "NON";
// 先手
var SENTE = "先手";
// 後手
var GOTE = "後手";
// 1回目クリック
var CLICK_FIRST = "1回目クリック";
// 2回目クリック
var CLICK_SECOND = "2回目クリック";
// 画像フォルダの場所
var FOLDER_IMG = "./img/";

// 序盤
var ZYOBAN = 1;
// 中盤
var CHUBAN = 2;
// 終盤
var SYUBAN = 3;

// 距離係数
var KEISU_KYORI = -500;
// 飛車間係数
var KEISU_HISYAKAN = 1300;
// 角間係数
var KEISU_KAKUKAN = 10;
// 金銀スクラム点数
var TENSU_KINGINSCRAM = 500;
// 王の遠さ点数
var TENSU_TOOSA = 70;
// AI待ち時間(ミリ秒)
var TIME_AI_WAIT = 100;
// 詰みの深さ
var CNT_TUMI = 2;

// 戦法 棒銀
var SENPOU_BOUGIN = 0;
// 戦法 四間飛車
var SENPOU_SIKENBISHA = 1;
// 囲い 船囲い
var KAKOI_FUNAGAKOI = 0;
// 囲い カニ囲い
var KAKOI_KANIGAKOI = 1;
// 囲い 美濃囲い
var KAKOI_MINOGAKOI = 2;

// メッセージ ようこそ
var MSG_WELCOME = "よろしくお願いします。<br />あなたが先手です。";
// メッセージ 考え中
var MSG_THINKING = "うーーーん……";
// メッセージ 考え中 勝ちの手
var MSG_WIN = "";
// メッセージ 考え中 逃げの手
var MSG_ESCAPE = "く、……";
// メッセージ 考え中 詰ませる
var MSG_TUMASERU = "これでどうでしょう。";
// メッセージ 考え中 駒得
var MSG_KOMADOKU = "こう指すとこうなって……";
// メッセージ 考え中 成り防ぎ
var MSG_NARIFUSEGI = "く、……こうだっけ。";
// メッセージ 考え中 駒攻め
var MSG_KOMASEME = "こういう手はどうでしょう。";
// メッセージ 考え中 攻め
var MSG_SEME = "そいやっ。";
// メッセージ 考え中 突き捨て
var MSG_TUKISUTE = "突き捨てときますか。";
// メッセージ 考え中 定石
var MSG_ZYOSEKI = "ここはこうですね。";
// メッセージ 考え中 囲う
var MSG_KAKOU = "ここはこうですね。";
// メッセージ 考え中 手待ち
var MSG_TEMATI = "こうしておきましょう。";
// メッセージ あなたの勝ち
var MSG_WIN = "<span class=\"wintext\">You Win</span><br />参りました。あなたの勝ちです。";
// メッセージ あなたの負け
var MSG_LOSE = "<span class=\"losetext\">You Lose</span><br />私の勝ちです。また対戦しましょう。";
// メッセージ あなたの投了で負け
var MSG_LOSE_TOURYOU = "<span class=\"losetext\">You Lose</span><br />投了ですか。また対戦しましょう。";

// 画像 考え中
var IMG_THINKING = "./img/thinking.jpg";
// 画像 勝ち
var IMG_WIN = "./img/win.jpg";
// 画像 負け
var IMG_LOSE = "./img/lose.jpg";


// ------------------------------
// グローバル変数
// ------------------------------
// 将棋盤
var ban;
// 先手
var sente;
// 後手
var gote;
// 手番
var teban;
// クリック回数
var clickTime;
// fromVectol
var fromVectol;
// toVectol
var toVectol;
// 選択された駒
var selectedKoma;
// komadaiNum
var komadaiNum;
// ゲーム終了フラグ
var flgGameset;
// AI実行中フラグ
var isAiExecute;
// デバッグ出力フラグ
var flgDebugg = false;
// AI
var ai;
// 効果音(駒音)
var audio_komaoto;
// 効果音(勝ち)
var audio_win;
// 効果音(負け)
var audio_lose;


// ------------------------------
// 初期化
// ------------------------------
function init() {
    // 将棋盤を生成
    ban = new Ban();
    // 先手を生成
    sente = new Human(SENTE);
    // 後手を生成
    gote = new Human(GOTE);
    // 手番を先手に設定
    teban = SENTE;
    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;
    // fromVectolを初期化
    fromVectol = new Vectol(-99, -99);
    // toVectolを初期化
    toVectol = new Vectol(-99, -99);
    // komadaiNumを初期化
    komadaiNum = -1;
    // ゲーム終了フラグをfalseに設定
    flgGameset = false;
    // 選択された駒を初期化
    selectedKoma = new Non();
    // AI実行中フラグを初期化
    isAiExecute = false;
    // AIを生成
    ai = new Ai5(GOTE);
    // 音楽ファイルの読み込み
    loadAudio();
    
    // メッセージを初期化
    printMsg(MSG_WELCOME);
    // 画像を初期化
    chgCharImg(IMG_THINKING);
    
    // htmlを描画
    ban.viewHtml();
}


// ------------------------------
// 盤上をクリックした時の処理
// ------------------------------
function onClickBan(x, y) {
    if (flgGameset) {
        return;
    }
    
    if (clickTime === CLICK_FIRST) {
        if (isAiExecute) {
            onClickBanFirstProcessAi(x, y);
        } else {
            onClickBanFirstProcessHuman(x, y);
        }
    } else {
        if (isAiExecute) {
            onClickBanSecondProcessAi(x, y);
        } else {
            onClickBanSecondProcessHuman(x, y);
        }
    }
    
    if (isGameSet()) {
        flgGameset = true;
    }
}

// ------------------------------
// 駒台をクリックした時の処理
// ------------------------------
function onClickKomadai(i, muki) {
    if (flgGameset) {
        return;
    }
    
    if (clickTime === CLICK_FIRST) {
        if (isAiExecute) {
            onClickKomadaiFirstProcessAi(i, muki);
        } else {
            onClickKomadaiFirstProcessHuman(i, muki);
        }
    } else {
        if (isAiExecute) {
            onClickKomadaiSecondProcessAi(i, muki);
        } else {
            onClickKomadaiSecondProcessHuman(i, muki);
        }
    }
}


// ------------------------------
// 1回目盤上クリック時の人間の処理
// ------------------------------
function onClickBanFirstProcessHuman(x, y) {
    // 駒を取得
    selectedKoma = ban.field[x][y];

    // 駒がない場合は何もしない
    if (selectedKoma.html === NONSTR) {
        return;
    }

    // fromVectolにxyを設定
    fromVectol = new Vectol(x, y);

    // クリック回数を2回目クリックに設定
    clickTime = CLICK_SECOND;

    // 指定された場所の駒の画像を隠してカーソルを選択した駒に変更
    ban.hideVectolPositionBan(fromVectol);
    ban.changeCursol(selectedKoma);
}

// ------------------------------
// 1回目盤上クリック時のAIの処理
// ------------------------------
function onClickBanFirstProcessAi(x, y) {
    // 駒を取得
    selectedKoma = ban.field[x][y];

    // 駒がない場合は何もしない
    if (selectedKoma.html === NONSTR) {
        return;
    }

    // fromVectolにxyを設定
    fromVectol = new Vectol(x, y);

    // クリック回数を2回目クリックに設定
    clickTime = CLICK_SECOND;
}

// ------------------------------
// 2回目盤上クリック時の人間の処理
// ------------------------------
function onClickBanSecondProcessHuman(x, y) {
    flashTxt();
    
    // 手番でクリックした人を判定
    var human;
    if (teban === SENTE) {
        human = sente;
    } else {
        human = gote;
    }
    
    // toVectolにxyを設定
    toVectol = new Vectol(x, y);

    // 駒を置くもしくは動かすことができたかのフラグ
    var canActionFlg = false;

    if (komadaiNum !== -1) {
        // 駒台の駒を選択していた場合

        // 駒台の駒を置く
        canActionFlg = human.put(komadaiNum, toVectol, selectedKoma, ban);
    } else {
        // 盤上の駒を選択していた場合

        // 駒を動かす
        canActionFlg = human.move(fromVectol, toVectol, selectedKoma, ban);
    }

    if (canActionFlg) {
        // 動かせた場合
        
        // 効果音(駒音)を再生
        playAudioKomaoto();
        
        // 動いた先の枠の色を目立たせる
        chgParentClassToStrong(toVectol.x, toVectol.y);
        
        // 棋譜を追加
        ban.pushKifu(teban, toVectol, toVectol, selectedKoma, komadaiNum);

        ban.viewHtml();

        // 手番を変更
        if (teban === SENTE) {
            teban = GOTE;
        } else {
            teban = SENTE;
        }
        
        // ゲーム終了の場合は以降の処理は実施しない
        if (isGameSet()) {
            return;
        }
        
        // AI実行中フラグをtrueに設定
        isAiExecute = true;
        blockUserClick();

        // 指定された時間後にAIを実行する
        printMsg(MSG_THINKING);
        debbugApendTxt("");
        setTimeout(execAi, TIME_AI_WAIT);
    } else {
        // 動かせなかった場合

        ban.viewHtml();
    }

    // komadaiNumを初期化
    komadaiNum = -1;
    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;

    // カーソルを元に戻す
    ban.reverseCursol();
}

// ------------------------------
// 2回目盤上クリック時のAIの処理
// ------------------------------
function onClickBanSecondProcessAi(x, y) {
    // クリックしたAI
    var humanAi;
    
    // 手番でクリックしたAIを判定
    humanAi = new HumanAi(teban);
    
    // toVectolにxyを設定
    toVectol = new Vectol(x, y);

    // 駒を置くもしくは動かすことができたかのフラグ
    var canActionFlg = false;

    if (komadaiNum !== -1) {
        // 駒台の駒を選択していた場合

        // 駒台の駒を置く
        canActionFlg = humanAi.put(komadaiNum, toVectol, selectedKoma, ban);
    } else {
        // 盤上の駒を選択していた場合

        // 駒を動かす
        // TODO とりあえず全て成る動きとしておく
        canActionFlg = humanAi.move(fromVectol, toVectol, selectedKoma, ban, true);
    }

    if (canActionFlg) {
        // 動かせた場合
        
        // 効果音を再生
        playAudioKomaoto();
        
        // 動いた先の枠の色を目立たせる
        chgParentClassToStrong(toVectol.x, toVectol.y);
        
        // 棋譜を追加
        ban.pushKifu(teban, toVectol, toVectol, selectedKoma, komadaiNum);

        ban.viewHtml();

        // 手番を変更
        if (teban === SENTE) {
            teban = GOTE;
        } else {
            teban = SENTE;
        }

        isAiExecute = false;
        releaseBlockUserClick();
    } else {
        // 動かせなかった場合

        ban.viewHtml();
    }

    // komadaiNumを初期化
    komadaiNum = -1;
    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;
}

// ------------------------------
// 1回目駒台クリック時の人間の処理
// ------------------------------
function onClickKomadaiFirstProcessHuman(i, muki) {
    flashTxt();
    
    if (muki === SENTE) {
        // 先手の駒台の場合、先手の駒台から駒を取得
        selectedKoma = ban.komadaiSente[i];
    } else {
        // 後手の駒台の場合、後手の駒台から駒を取得
        selectedKoma = ban.komadaiGote[i];
    }

    // 駒がない場合は何もしない
    if (selectedKoma.html === NONSTR) {
        return;
    }

    // komadaiNumにクリックした箇所の番号(i)を設定
    komadaiNum = i;

    // クリック回数を2回目クリックに設定
    clickTime = CLICK_SECOND;

    // 指定された場所の駒の画像を隠してカーソルを選択した駒に変更
    ban.hideVectolPositionKomadai(komadaiNum, teban);
    ban.changeCursol(selectedKoma);
}

// ------------------------------
// 1回目駒台クリック時のAIの処理
// ------------------------------
function onClickKomadaiFirstProcessAi(i, muki) {
    if (muki === SENTE) {
        // 先手の駒台の場合、先手の駒台から駒を取得
        selectedKoma = ban.komadaiSente[i];
    } else {
        // 後手の駒台の場合、後手の駒台から駒を取得
        selectedKoma = ban.komadaiGote[i];
    }

    // 駒がない場合は何もしない
    if (selectedKoma.html === NONSTR) {
        return;
    }

    // komadaiNumにクリックした箇所の番号(i)を設定
    komadaiNum = i;

    // クリック回数を2回目クリックに設定
    clickTime = CLICK_SECOND;
}

// ------------------------------
// 2回目駒台クリック時の人間の処理
// ------------------------------
function onClickKomadaiSecondProcessHuman(i, muki) {
    // komadaiNumを初期化
    komadaiNum = -1;

    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;

    // カーソルを元に戻す
    ban.reverseCursol();

    // htmlを描画
    ban.viewHtml();
}

// ------------------------------
// 2回目駒台クリック時のAIの処理
// ------------------------------
function onClickKomadaiSecondProcessAi(i, muki) {
    // komadaiNumを初期化
    komadaiNum = -1;

    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;

    // htmlを描画
    ban.viewHtml();
}


// ------------------------------
// ゲーム終了判定
// ------------------------------
function isGameSet() {
    if (isGameSetSenteWin(ban) || isGameSetGoteWin(ban)) {
        return true;
    }
    
    return false;
}

// ------------------------------
// ゲーム終了判定(先手)
// ------------------------------
function isGameSetSenteWin(inBan) {
    // 先手の駒台に王が存在するか判定
    for (i = 0; i < inBan.komadaiSente.length; i++) {
        if (inBan.komadaiSente[i].html === "王") {
            playAudioWin();
            printMsg(MSG_WIN);
            chgCharImg(IMG_WIN);
            return true;
        }
    }
    
    return false;
}

// ------------------------------
// ゲーム終了判定(後手)
// ------------------------------
function isGameSetGoteWin(inBan) {
    // 後手の駒台に王が存在するか判定
    for (i = 0; i < inBan.komadaiGote.length; i++) {
        if (inBan.komadaiGote[i].html === "王") {
            playAudioLose();
            printMsg(MSG_LOSE);
            chgCharImg(IMG_LOSE);
            return true;
        }
    }
    
    return false;
}

// ------------------------------
// 相手の手番の取得
// ------------------------------
function getAiteTeban(teban) {
    if (teban === SENTE) {
        return GOTE;
    } else {
        return SENTE;
    }
}

// ------------------------------
// メッセージ出力
// ------------------------------
function printMsg(txt) {
    document.getElementById("message").innerHTML = txt;
}

// ------------------------------
// AIを実行する
// ------------------------------
var execAi = function () {
    ai.execute();
};

// ------------------------------
// デバッグ出力(append)
// ------------------------------
function debbugApendTxt(txt) {
    if (flgDebugg) {
        document.getElementById("message").innerHTML += txt;
        document.getElementById("message").innerHTML += "<br>";
    }
}

// ------------------------------
// デバッグ出力(append)改行なし
// ------------------------------
function debbugApendTxtNonBr(txt) {
    if (flgDebugg) {
        document.getElementById("message").innerHTML += txt;
    }
}

// ------------------------------
// デバッグ出力
// ------------------------------
function debbug() {
    if (flgDebugg) {
        ban.printKifu();
    }
}

// ------------------------------
// テキストエリアフラッシュ
// ------------------------------
function flashTxt() {
    document.getElementById("message").innerHTML = "";
}

// ------------------------------
// クリック無効化
// ------------------------------
function blockUserClick() {
    for (var i = 0;i < 40;i++) {
        document.getElementById("komadai_sente_" + String(i)).disabled = "true";
        document.getElementById("komadai_gote_" + String(i)).disabled = "true";
    }
    for (var i = 0;i < 9;i++) {
        for (var j = 0;j < 9;j++) {
            document.getElementById(String(i) + "-" + String(j)).disabled = "true";
        }
    }
}

// ------------------------------
// クリック無効化を解除
// ------------------------------
function releaseBlockUserClick() {
    for (var i = 0;i < 40;i++) {
        document.getElementById("komadai_sente_" + String(i)).disabled = "";
        document.getElementById("komadai_gote_" + String(i)).disabled = "";
    }
    for (var i = 0;i < 9;i++) {
        for (var j = 0;j < 9;j++) {
            document.getElementById(String(i) + "-" + String(j)).disabled = "";
        }
    }
}

// ------------------------------
// 親要素の外枠を目立たせる
// ------------------------------
function chgParentClassToStrong(x, y) {
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            var target = document.getElementById(String(i) + "-" + String(j));
            var parent = target.parentNode;
            parent.classList.remove('selected');
        }
    }
    
    var target = document.getElementById(String(x) + "-" + String(y));
    var parent = target.parentNode;
    parent.classList.add('selected');
}

// ------------------------------
// 選択をキャンセル
// ------------------------------
function cancelSelected() {
    // 再描画
    ban.viewHtml();
    // komadaiNumを初期化
    komadaiNum = -1;
    // クリック回数を1回目クリックに設定
    clickTime = CLICK_FIRST;
    // カーソルを元に戻す
    ban.reverseCursol();
}

// ------------------------------
// 音楽を読み込む
// ------------------------------
function loadAudio() {
    audio_komaoto = new Audio("./komaoto.mp3");
    audio_win = new Audio("./win.mp3");
    audio_lose = new Audio("./lose.mp3");
}

// ------------------------------
// 効果音(駒音)を再生
// ------------------------------
function playAudioKomaoto() {
    audio_komaoto.play();
}

// ------------------------------
// 効果音(勝ち)を再生
// ------------------------------
function playAudioWin() {
    audio_win.play();
}

// ------------------------------
// 効果音(負け)を再生
// ------------------------------
function playAudioLose() {
    audio_lose.play();
}

// ------------------------------
// 投了する
// ------------------------------
function doTouryou() {
    playAudioLose();
    printMsg(MSG_LOSE_TOURYOU);
    chgCharImg(IMG_WIN);
    flgGameset = true;
}

// ------------------------------
// 画像を変更する
// ------------------------------
function chgCharImg(src) {
    document.getElementById("chuntaimg").src = src;
}

// ------------------------------
// 確認ダイアログを表示する
// キャンセルされたらrerurn false
// ------------------------------
function doConfirm(txt) {
    if (window.confirm(txt)) {
        return;
    }
    
    exit;
}
