// ------------------------------
// HumanAi
// AIの人間動作に該当する部分を定義。
// 
// ------------------------------
// コンストラクタ
var HumanAi = function(teban){
    // 手番
    this.teban = teban;
    // util
    this.util = new HumanUtil(this.teban);
};

HumanAi.prototype = {
    // 駒台の駒を置く
    put: function(komadaiNum, toVectol, koma, tugiban) {
        // 盤をバックアップ
        var banBk = new Ban();
        banBk.setBan(tugiban);
        
        // 自分の駒か判定
        if (koma.muki !== this.teban) {
            return false;
        }
        
        // 置く箇所に自分の駒があるか判定
        if (this.util.isExistZibunNoKomaAtToVectol(toVectol, tugiban)) {
            return false;
        }
        
        // 置く箇所に敵の駒があるか判定
        if (this.util.isExistAiteNoKomaAtToVectol(toVectol, tugiban)) {
            return false;
        }
        
        // 置ける場所か判定
        if (!this.util.isPutPosition(toVectol, koma)) {
            return false;
        }
        
        // 二歩を判定
        if (this.util.isNifu(toVectol, koma, tugiban)) {
            return false;
        }
        
        // 駒台の駒を無くす処理
        this.util.removeKomadai(komadaiNum, tugiban);
        
        // 駒を置く処理
        this.util.doOku(toVectol, koma, tugiban);
        
        // 打ち歩詰めを判定
        // 打ち歩詰めとなる場合は局面を元に戻す
        if (this.util.isUtiFuTume(koma, tugiban)) {
            // 盤をバックアップした状態に戻す
            tugiban.setBan(banBk);
            
            return false;
        }
        
        // AI履歴に履歴を追加
        tugiban.pushAiHistory(this.teban, toVectol, toVectol, koma, komadaiNum);
        
        return true;
    },
    
    // 駒を動かす
    move: function (fromVectol, toVectol, koma, tugiban, isNaru) {
        // 駒を動かせるか判定
        if (!this.util.canMove(fromVectol, toVectol, koma)) {
            return false;
        }
        
        // 間に駒があるか判定
        if (this.util.isExistAidaKoma(fromVectol, toVectol, koma, tugiban)) {
            return false;
        }
        
        // 置く箇所に自分の駒があるか判定
        if (this.util.isExistZibunNoKomaAtToVectol(toVectol, tugiban)) {
            return false;
        }
        
        // 置く箇所に敵の駒があるか判定
        if (this.util.isExistAiteNoKomaAtToVectol(toVectol, tugiban)) {
            // 敵の駒を駒台に乗せる
            this.util.addAiteNoKoma(toVectol, tugiban);
        }
        
        // 成れるか判定
        if (this.util.canNaru(fromVectol, toVectol, koma)) {
            // 成れる駒の場合の処理 成らない場合は駒を置く処理を実施
            this.doNaru(toVectol, koma, tugiban, isNaru);
        } else {
            // 成れない駒の場合の処理
            // 駒を置く
            this.util.doOku(toVectol, koma, tugiban);
        }
        
        // 動かし元の駒を無くす処理
        this.util.doRemoveMotoNoKoma(fromVectol, tugiban);
        
        // AI履歴に履歴を追加
        tugiban.pushAiHistory(this.teban, fromVectol, toVectol, tugiban.field[toVectol.x][toVectol.y], -1);
        
        return true;
    },
    
    // 成る処理
    doNaru: function (toVectol, koma, tugiban, isNaru) {
        if (isNaru) {
            // 成る場合の処理
            if (koma.html === "歩") {
                tugiban.field[toVectol.x][toVectol.y] = new To(this.teban);
            }
            if (koma.html === "香") {
                tugiban.field[toVectol.x][toVectol.y] = new NariKyou(this.teban);
            }
            if (koma.html === "桂") {
                tugiban.field[toVectol.x][toVectol.y] = new NariKeima(this.teban);
            }
            if (koma.html === "銀") {
                tugiban.field[toVectol.x][toVectol.y] = new NariGin(this.teban);
            }
            if (koma.html === "角") {
                tugiban.field[toVectol.x][toVectol.y] = new Uma(this.teban);
            }
            if (koma.html === "飛") {
                tugiban.field[toVectol.x][toVectol.y] = new Ryu(this.teban);
            }
        } else {
            // 成らない場合の処理
            // 駒を置く
            this.util.doOku(toVectol, koma, tugiban);
        }
    },
    
    // 駒を動かす
    // ただし、うごかし場所に自分の駒があった場合はOKとする
    moveToMyKoma: function (fromVectol, toVectol, koma, tugiban, isNaru) {
        // 駒を動かせるか判定
        if (!this.util.canMove(fromVectol, toVectol, koma)) {
            return false;
        }
        
        // 間に駒があるか判定
        if (this.util.isExistAidaKoma(fromVectol, toVectol, koma, tugiban)) {
            return false;
        }
        
        // 置く箇所に自分の駒があるか判定
        if (this.util.isExistZibunNoKomaAtToVectol(toVectol, tugiban)) {
            // OKとする
        }
        
        // 置く箇所に敵の駒があるか判定
        if (this.util.isExistAiteNoKomaAtToVectol(toVectol, tugiban)) {
            // 敵の駒を駒台に乗せる
            this.util.addAiteNoKoma(toVectol, tugiban);
        }
        
        // 成れるか判定
        if (this.util.canNaru(fromVectol, toVectol, koma)) {
            // 成れる駒の場合の処理 成らない場合は駒を置く処理を実施
            this.doNaru(toVectol, koma, tugiban, isNaru);
        } else {
            // 成れない駒の場合の処理
            // 駒を置く
            this.util.doOku(toVectol, koma, tugiban);
        }
        
        // 動かし元の駒を無くす処理
        this.util.doRemoveMotoNoKoma(fromVectol, tugiban);
        
        return true;
    }
};