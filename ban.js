// ------------------------------
// 将棋盤
// 将棋盤を定義。
// 
// ------------------------------
// コンストラクタ
var Ban = function(){
    // フィールド
    this.field = new Array(9);
    
    // 駒台 先手
    this.komadaiSente = new Array(40);
    
    // 駒台 後手
    this.komadaiGote = new Array(40);
    
    // フィールドを生成
    for (var i = 0; i < 9; i++) {
        this.field[i] = new Array(9);
    }
    
    // 全ての箇所に駒無を配置
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            this.field[i][j] = new Non();
        }
    }
    
    // 歩を配置
    for (var i = 0; i < 9; i++) {
        this.field[i][2] = new Fu(GOTE);
        this.field[i][6] = new Fu(SENTE);
    }
    
    // 香を配置
    this.field[0][0] = new Kyou(GOTE);
    this.field[8][0] = new Kyou(GOTE);
    this.field[0][8] = new Kyou(SENTE);
    this.field[8][8] = new Kyou(SENTE);
    
    // 桂を配置
    this.field[1][0] = new Keima(GOTE);
    this.field[7][0] = new Keima(GOTE);
    this.field[1][8] = new Keima(SENTE);
    this.field[7][8] = new Keima(SENTE);
    
    // 銀を配置
    this.field[2][0] = new Gin(GOTE);
    this.field[6][0] = new Gin(GOTE);
    this.field[2][8] = new Gin(SENTE);
    this.field[6][8] = new Gin(SENTE);
    
    // 金を配置
    this.field[3][0] = new Kin(GOTE);
    this.field[5][0] = new Kin(GOTE);
    this.field[3][8] = new Kin(SENTE);
    this.field[5][8] = new Kin(SENTE);
    
    // 角を配置
    this.field[7][1] = new Kaku(GOTE);
    this.field[1][7] = new Kaku(SENTE);
    
    // 飛を配置
    this.field[1][1] = new Hisha(GOTE);
    this.field[7][7] = new Hisha(SENTE);
    
    // 王を配置
    this.field[4][0] = new Ou(GOTE);
    this.field[4][8] = new Ou(SENTE);
    
    // 全ての先手および後手の駒台の箇所に駒無を配置
    for (var i = 0; i < 40; i++) {
        this.komadaiSente[i] = new Non();
        this.komadaiGote[i] = new Non();
    }
    
    // AI履歴 手番
    this.historyAiTeban = new Array();
    // AI履歴 fromVectol
    this.historyAiFromVectol = new Array();
    // AI履歴 toVectol
    this.historyAiToVectol = new Array();
    // AI履歴 駒
    this.historyAiKoma = new Array();
    // AI履歴 打つ
    this.historyKomadaiNum = new Array();
    
    // 棋譜 手番
    this.kifuTeban = new Array();
    // 棋譜 fromVectol
    this.kifuFromVectol = new Array();
    // 棋譜 toVectol
    this.kifuToVectol = new Array();
    // 棋譜 駒
    this.kifuKoma = new Array();
    // 棋譜 打つ
    this.kifuKomadaiNum = new Array();
};


Ban.prototype = {
    // htmlを出力する
    viewHtml: function () {
        // フィールドを出力
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                // 画像を変更する
                document.getElementById(String(i) + "-" + String(j)).src = this.field[i][j].imgsrc;
            }
        }
        
        // 駒台を出力
        for (var i = 0; i < 40; i++) {
            // 画像を変更する
            document.getElementById("komadai_sente_" + String(i)).src = this.komadaiSente[i].imgsrc;
            document.getElementById("komadai_gote_" + String(i)).src = this.komadaiGote[i].imgsrc;
        }
    },
    
    // 指定された場所の駒の画像を隠す
    hideVectolPositionBan: function (vectol) {
        document.getElementById(String(vectol.x) + "-" + String(vectol.y)).src = FOLDER_IMG + "non.png";
    },
    
    // 指定された場所の駒台の画像を隠す
    hideVectolPositionKomadai: function (komadaiNum, teban) {
        if (teban === SENTE) {
            document.getElementById("komadai_sente_" + String(komadaiNum)).src = FOLDER_IMG + "non.png";
        } else {
            document.getElementById("komadai_gote_" + String(komadaiNum)).src = FOLDER_IMG + "non.png";
        }
    },
    
    // カーソルを変更する
    changeCursol: function (koma) {
        var imgUrl = koma.imgsrc;
        document.body.style.cursor = "url(\"" + imgUrl + "\") 10 10,auto";
    },
    
    // カーソルを元に戻す
    reverseCursol: function () {
        document.body.style.cursor = "auto";
    },
    
    // NONSTRでない駒台の配列を取得する
    getKomadaiArray: function (teban) {
        var array = new Array();
        
        if (teban === SENTE) {
            for (var i = 0; i < this.komadaiSente.length; i++) {
                if (this.komadaiSente[i].html !== NONSTR) {
                    array.push(this.komadaiSente[i]);
                }
            }
        } else {
            for (var i = 0; i < this.komadaiGote.length; i++) {
                if (this.komadaiGote[i].html !== NONSTR) {
                    array.push(this.komadaiGote[i]);
                }
            }
        }
        
        return array;
    },
    
    // 盤の状態を引数の盤の状態に戻す
    setBan: function (inBan) {
        // 盤からコピー(値渡し)
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                this.field[i][j] = inBan.field[i][j];
            }
        }
        
        // 駒台からコピー(値渡し)
        for (var i = 0; i < 40; i++) {
            this.komadaiSente[i] = inBan.komadaiSente[i];
            this.komadaiGote[i] = inBan.komadaiGote[i];
        }
    },
    
    // 引数の盤について手番側が勝ちかどうか判定する
    isTumi: function(teban) {
        // 王手しているか判定
        var outeArray = this.getOuteArray(teban);
        
        if (outeArray.length === 0) {
            return false;
        }
        
        // 相手が王の移動で逃げられるか判定
        if (this.canEscapeOuIdou(getAiteTeban(teban))) {
            return false;
        }
        
        // 相手が間駒できるか判定
        if (this.canEscapeAigoma(getAiteTeban(teban))) {
            return false;
        }
      
        // 相手が王手の駒を取れるか判定
        if (this.canEscapeToru(getAiteTeban(teban))) {
            return false;
        }
        
        return true;
    },
    
    // 引数の手番側が王手をかけている駒の配列を取得する
    getOuteArray: function (teban) {
        // 王手している配列
        var outeKomaArray = new Array();
        
        // 現在の状態をコピーした継ぎ盤を作成
        var tugiban = this.clone();
        
        // AIを作成
        var ai = new HumanAi(teban);
        
        // 相手の王のベクトルを取得
        var toVectol = this.getOuVectol(getAiteTeban(teban));
        
        // 自分の駒かつ相手玉まで動かせる駒を見つける
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                // 手番の駒か判定
                if (tugiban.field[i][j].muki === teban) {
                    // 相手玉まで動かせるか判定
                    if (ai.move(new Vectol(i, j), toVectol, tugiban.field[i][j], tugiban, true) ) {
                        // 王手している駒の配列に加える
                        // 加える駒は元の盤からであることに注意
                        outeKomaArray.push(this.field[i][j]);
                    }
                }
            }
        }
        
        return outeKomaArray;
    },
    
    // 引数の手番側が王手をかけている駒のベクトル配列を取得する
    getOuteVectolArray: function (teban) {
        // 王手している配列
        var outeVectolArray = new Array();
        
        // AIを作成
        var ai = new HumanAi(teban);
        
        // 相手の王のベクトルを取得
        var toVectol = this.getOuVectol(getAiteTeban(teban));
        
        // 自分の駒かつ相手の玉まで動かせる駒を見つける
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                // 継ぎ盤を作成(または再度新規から作成)
                var tugiban = this.clone();
                
                // 自分の駒か判定
                if (tugiban.field[i][j].muki === teban) {
                    // 相手の玉まで動かせるか判定
                    if (ai.move(new Vectol(i, j), toVectol, tugiban.field[i][j], tugiban, false) ) {
                        // 王手しているベクトルの配列に加える
                        outeVectolArray.push(new Vectol(i, j));
                    }
                }
            }
        }
        
        return outeVectolArray;
    },
    
    // 引数の手番の玉のベクトルを取得する
    getOuVectol: function (teban) {
        var vectol = new Vectol(-1, -1);
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (this.field[i][j].html === "王" && this.field[i][j].muki === teban) {
                    vectol = new Vectol(i, j);
                    return vectol;
                }
            }
        }
        
        return vectol;
    },
    
    // 引数の手番の飛車のベクトルを取得する
    getHishaVectol: function (teban) {
        var vectol = new Vectol(-1, -1);
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (this.field[i][j].html === "飛" && this.field[i][j].muki === teban) {
                    vectol = new Vectol(i, j);
                    return vectol;
                }
            }
        }
        
        return vectol;
    },
    
    // 盤のクローンを作成する
    // 参照渡しではないことに注意する
    clone: function () {
        // 継ぎ盤
        var clone = new Ban();
        
        // 盤からコピー(値渡し)
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                clone.field[i][j] = this.field[i][j];
            }
        }
        
        // 駒台からコピー(値渡し)
        for (var i = 0; i < 40; i++) {
            clone.komadaiSente[i] = this.komadaiSente[i];
            clone.komadaiGote[i] = this.komadaiGote[i];
        }
        
        // 評価値からコピー(値渡し)
        clone.hyokati = this.hyokati;

        // AI履歴 手番からコピー(値渡し)
        for (var i = 0; i < this.historyAiTeban.length; i++) {
            clone.historyAiTeban[i] = this.historyAiTeban[i];
        }
        
        // AI履歴 fromVectolからコピー(値渡し)
        for (var i = 0; i < this.historyAiFromVectol.length; i++) {
            var vectol = new Vectol(this.historyAiFromVectol[i].x, this.historyAiFromVectol[i].y);
            
            clone.historyAiFromVectol.push(vectol);
        }
        
        // AI履歴 toVectolからコピー(値渡し)
        for (var i = 0; i < this.historyAiToVectol.length; i++) {
            var vectol = new Vectol(this.historyAiToVectol[i].x, this.historyAiToVectol[i].y);
            
            clone.historyAiToVectol.push(vectol);
        }
        
        // AI履歴 駒からコピー(値渡し)
        for (var i = 0; i < this.historyAiKoma.length; i++) {
            clone.historyAiKoma[i] = this.historyAiKoma[i];
        }
        
        // AI履歴 打つからコピー(値渡し)
        for (var i = 0; i < this.historyKomadaiNum.length; i++) {
            clone.historyKomadaiNum[i] = this.historyKomadaiNum[i];
        }
        
        return clone;
    },
    
    // 引数の手番が王の移動で逃げられるか判定
    canEscapeOuIdou: function(teban) {
        // AIを作成
        var ai = new HumanAi(teban);
        
        // 王のベクトルを取得
        var vectolOu = this.getOuVectol(teban);
        
        for (var i = 0; i < this.field[vectolOu.x][vectolOu.y].vectol.length; i++) {
            // 継ぎ盤を作成(または再度新規から作成)
            var tugiban = this.clone();
            
            // 移動後のベクトルを算出
            var x = vectolOu.x + tugiban.field[vectolOu.x][vectolOu.y].vectol[i].x;
            var y = vectolOu.y + tugiban.field[vectolOu.x][vectolOu.y].vectol[i].y;
            
            if ( !( 0 <= x && x <= 8 && 0 <= y && y <= 8 ) ) {
                // 盤外になる場合は以降の処理は行わない
                continue;
            }
            
            // 継ぎ盤で駒を動かす
            var result = ai.move(vectolOu, new Vectol(x, y), tugiban.field[vectolOu.x][vectolOu.y], tugiban, false);
            
            // 駒が動かせたか判定
            if (result) {
                // 動かした後で相手に王手されているか判定
                var outeVectolArray = tugiban.getOuteVectolArray(getAiteTeban(teban));
                
                if (outeVectolArray.length === 0) {
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // 引数の手番が間駒できるか判定
    canEscapeAigoma: function(teban) {
        // 継ぎ盤を作成
        var tugiban = this.clone();
        
        // 王のベクトルを取得
        var vectolOu = tugiban.getOuVectol(teban);
        
        // 相手の駒かつ自分の玉まで動かせる駒を見つける
        var outeVectolArray = tugiban.getOuteVectolArray(getAiteTeban(teban));
        
        // 自玉に王手している駒全てに対して間駒ができるか判定
        for (var k = 0; k < outeVectolArray.length; k++) {
            if (tugiban.canPutKoma(outeVectolArray[k], vectolOu, teban) ) {
                // 駒が置けた場合はtrue
                return true;
            }
        }
        
        return false;
    },
    
    // 引数の手番が引数の位置で間駒できるか判定
    canPutKoma: function(fromVectol, toVectol, teban) {
        // 継ぎ盤を作成
        var tugiban = this.clone();
        
        // AIを作成
        var ai = new HumanAi(teban);
        
        // fromVectolとtoVectolからベクトルを算出
        var vectol = new Vectol(toVectol.x - fromVectol.x, toVectol.y - fromVectol.y);
        
        // 算出したベクトルと駒の種類から関数ベクトルを算出
        var vectolKansu = ai.util.calcVectolKansu(vectol);

        // fromVectolとtoVectolから間のxy配列を算出
        var aidaXyArray = ai.util.calcAidaXyArray(fromVectol, toVectol, vectolKansu);
        
        // 間の配列が存在しない場合はfalse
        if (aidaXyArray.length === 0) {
            return false;
        }
        
        // 持ち駒を取得
        var motigomaArray = tugiban.getKomadaiArray(teban);
        
        // 間駒配列の場所に間駒できるか判定
        for (var j = 0; j < aidaXyArray.length; j++) {
            // 間駒(持ち駒)できるか判定
            for (var i = 0; i < motigomaArray.length; i++) {

                if (ai.put(i, aidaXyArray[j], motigomaArray[i], tugiban)) {
                    // 置ける場合true

                    return true;
                }
            }
            
            // 間駒(移動合い)できるか判定
            if (tugiban.canEscapeIdouAi(teban, aidaXyArray[j])) {
                // 可能な場合true

                return true;
            }
        }
        
        return false;
    },
    
    // 引数の手番が王手されている駒を取れるか判定
    canEscapeToru: function(teban) {
        // 継ぎ盤を作成
        var tugiban = this.clone();
        
        // AIを作成
        var ai = new HumanAi(teban);
        
        // 自玉に対して王手しているベクトル配列を取得
        var outeVectolArray = tugiban.getOuteVectolArray(getAiteTeban(teban));
        
        // 自分の駒かつ王手されている駒まで動かせる駒を見つける
        for (var k = 0; k < outeVectolArray.length; k++) {
            for (var i = 0; i < 9; i++) {
                for (var j = 0; j < 9; j++) {
                    // 自分の駒か判定
                    if (tugiban.field[i][j].muki === teban) {
                        // 継ぎ盤を再度作成する
                        tugiban = this.clone();
                        
                        // 王手されている駒まで動かせるか判定
                        if (ai.move(new Vectol(i, j), outeVectolArray[k], tugiban.field[i][j], tugiban, false)) {
                            // 動かした後で王手されていないか判定
                            var outeArray = tugiban.getOuteArray(getAiteTeban(teban));
                            
                            if (outeArray.length === 0) {
                                // 動かした後で王手されていなければtrue
                                return true;
                            }
                        }
                    }
                }
            }
        }
        
        return false;
    },
    
    // 間駒(移動合い)できるか判定
    canEscapeIdouAi: function(teban, toVectol) {
        // AIを作成
        var ai = new HumanAi(teban);
        
        // 自分の駒かつtoVectolまで動かせる駒を見つける
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                // 引数で受け取った盤をコピーした継ぎ盤を作成
                var tugiban = this.clone();
                
                // 自分の駒か判定
                if (tugiban.field[i][j].muki === teban) {
                    // toVectolまで動かせるか判定
                    if (ai.move(new Vectol(i, j), toVectol, tugiban.field[i][j], tugiban, false) ) {
                        // 動かせたらtrue
                        return true;
                    }
                }
            }
        }
        
        return false;
    },
    
    // AI履歴に追加する
    pushAiHistory: function(teban, fromVectol, toVectol, koma, komadaiNum) {
        this.historyAiTeban.push(teban);
        this.historyAiFromVectol.push(fromVectol);
        this.historyAiToVectol.push(toVectol);
        this.historyAiKoma.push(koma);
        this.historyKomadaiNum.push(komadaiNum);
    },
    
    // AI履歴を出力する
    printAiHistory: function() {
        debbugApendTxt("AI--------------------------");
        for (var i = 0; i < this.historyAiTeban.length; i++) {
            debbugApendTxt("手番: " + String(this.historyAiTeban[i]));
            debbugApendTxtNonBr("fromVectol x, y = "  + String(this.historyAiFromVectol[i].x) + ", " + String(this.historyAiFromVectol[i].y));
            debbugApendTxt("　toVectol x, y = "  + String(this.historyAiToVectol[i].x) + ", " + String(this.historyAiToVectol[i].y));
            debbugApendTxtNonBr("駒: "  + String(this.historyAiKoma[i].html));
            debbugApendTxt("　駒台no: "  + String(this.historyKomadaiNum[i]));
        }
        
        debbugApendTxt("------------------------------");
    },
    
    // 先頭のAI履歴fromVectolを取得する
    getFirstAiHistoryFromVectol: function() {
        return this.historyAiFromVectol[0];
    },
    
    // 先頭のAI履歴toVectolを取得する
    getFirstAiHistoryToVectol: function() {
        return this.historyAiToVectol[0];
    },
    
    // 先頭のAI履歴komaを取得する
    getFirstAiHistoryKoma: function() {
        return this.historyAiKoma[0];
    },
    
    // 先頭のAI履歴komadaiNumを取得する
    getFirstAiKomadaiNum: function() {
        return this.historyKomadaiNum[0];
    },
    
    // AI履歴を初期化する
    flashAiHistory: function() {
        this.historyAiTeban = new Array();
        this.historyAiFromVectol = new Array();
        this.historyAiToVectol = new Array();
        this.historyAiKoma = new Array();
        this.historyKomadaiNum = new Array();
    },
    
    // 引数の手番のベクトル配列を取得する
    getTargetTebanVectolArray: function(teban) {
        var resultArray = new Array();
        
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (this.field[i][j].muki === teban) {
                    resultArray.push(new Vectol(i, j));
                }
            }
        }
        
        return resultArray;
    },
    
    // 引数の手番のスコアの合計値を取得する
    getTargetTebanScoreSum: function(teban) {
        var result = 0;
        
        // 盤上
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (this.field[i][j].muki === teban) {
                    result += this.field[i][j].scoreBanzyou;
                }
            }
        }
        
        // 駒台
        var komadaiArray = this.getKomadaiArray(teban);
        for (var i = 0; i < komadaiArray.length; i++) {
            result += komadaiArray[i].scoreMotigoma;
        }
        
        return result;
    },
    
    // AI履歴存在判定
    isExistAiHistory: function() {
        if (this.historyAiTeban.length === 0) {
            return false;
        }
        
        return true;
    },
    
    // 棋譜に追加する
    pushKifu: function(teban, fromVectol, toVectol, koma, komadaiNum) {
        this.kifuTeban.push(teban);
        this.kifuFromVectol.push(fromVectol);
        this.kifuToVectol.push(toVectol);
        this.kifuKoma.push(koma);
        this.kifuKomadaiNum.push(komadaiNum);
    },
    
    // 棋譜を出力する
    printKifu: function() {
        debbugApendTxt("棋譜--------------------------");
        for (var i = 0; i < this.kifuTeban.length; i++) {
            debbugApendTxtNonBr("No. " + String(i + 1));
            debbugApendTxtNonBr("手番: " + String(this.kifuTeban[i]));
            debbugApendTxtNonBr("　toVectol x, y = "  + String(this.kifuToVectol[i].x) + ", " + String(this.kifuToVectol[i].y));
            debbugApendTxt("駒: "  + String(this.kifuKoma[i].html));
        }
        debbugApendTxt("------------------------------");
    }
};
