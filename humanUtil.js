// ------------------------------
// HumanUtil
// 人間の動作で汎用的なものを定義。
// 
// ------------------------------
// コンストラクタ
var HumanUtil = function(teban){
    // 手番
    this.teban = teban;
};

HumanUtil.prototype = {
    // 置ける場所か判定
    isPutPosition: function(toVectol, koma) {
        // 駒が歩香桂以外はtrue
        if (koma.html !== "歩" && koma.html !== "香" && koma.html !== "桂") {
            return true;
        }
        
        // 駒が歩香の場合
        if (koma.html === "歩" || koma.html === "香") {
            // 先手の場合は最上部の場合false
            if (this.teban === SENTE) {
                if (toVectol.y === 0) {
                    return false;
                }
            // 後手の場合は最下部の場合false
            } else {
                if (toVectol.y === 8) {
                    return false;
                }
            }
        // 駒が桂馬の場合
        } else {
            // 先手の場合は2より上の場合false
            if (this.teban === SENTE) {
                if (toVectol.y < 2) {
                    return false;
                }
            // 後手の場合は6より下の場合false
            } else {
                if (toVectol.y > 6) {
                    return false;
                }
            }
        }
        
        return true;
    },
    
    // 二歩を判定
    isNifu: function(toVectol, koma, tugiban) {
        // 歩以外の場合はfalse
        if (koma.html !== "歩") {
            return false;
        }
        
        // 駒のy座標上に自分の歩が存在する場合、true
        for (var i = 0; i < 9; i++) {
            if (tugiban.field[toVectol.x][i].html === "歩" && tugiban.field[toVectol.x][i].muki === this.teban) {
                return true;
            }
        }
        
        return false;
    },
    
    // 駒台の駒を無くす処理
    removeKomadai: function (komadaiNum, tugiban) {
        if (this.teban === SENTE) {
            tugiban.komadaiSente[komadaiNum] = new Non();
        } else {
            tugiban.komadaiGote[komadaiNum] = new Non();
        }
        
        // 隙間がなくなるようシフトする
        this.shiftKomadaiArray();
    },
    
    
    
    // 駒を動かせるか判定
    canMove: function (fromVectol, toVectol, koma) {
        // 駒が自分の駒か判定
        if (this.teban !== koma.muki) {
            // 自分の駒でない場合はfalse
            return false;
        }
        
        // fromVectolにkomaのvectolを加えた結果がtoVectolになるか調べる
        for (var i = 0; i < koma.vectol.length; i++) {
            var x = fromVectol.x + koma.vectol[i].x;
            var y = fromVectol.y + koma.vectol[i].y;
            
            if (x === toVectol.x && y === toVectol.y) {
                return true;
            }
        }
        
        return false;        
    },
    
    // 間に駒があるか判定
    isExistAidaKoma: function (fromVectol, toVectol, koma, tugiban) {
        // 駒が香角飛馬竜以外はfalse
        if (koma.html !== "香" && koma.html !== "角" && koma.html !== "飛" && koma.html !== "馬" && koma.html !== "竜") {
            return false;
        }
        
        // fromVectolとtoVectolからベクトルを算出
        var vectol = new Vectol(toVectol.x - fromVectol.x, toVectol.y - fromVectol.y);
        
        // 算出したベクトルと駒の種類から関数ベクトルを算出
        var vectolKansu = this.calcVectolKansu(vectol);

        // fromVectolとtoVectolから間のxy配列を算出
        var aidaXyArray = this.calcAidaXyArray(fromVectol, toVectol, vectolKansu);
        
        // 算出した間のxy配列に駒があるか判定
        for (var i = 0; i < aidaXyArray.length; i++) {
            if (tugiban.field[ aidaXyArray[i].x ][ aidaXyArray[i].y ].html !== NONSTR) {
                return true;
            }
        }
        
        return false;        
    },
    
    // 算出したベクトルと駒の種類から関数ベクトルを算出
    // ただし、厳密なベクトルではなく、各座標の絶対値を1にする処理を行う
    calcVectolKansu: function (vectol) {
        var vectolKansu;
        var x = 0;
        var y = 0;
        
        if (vectol.x > 0) {
            x = vectol.x / vectol.x;
        } else if (vectol.x < 0) {
            // 負のベクトルの場合は掛け合わせて正となることを防ぐ
            x = (vectol.x / vectol.x) * -1;
        }
        
        if (vectol.y > 0) {
            y = vectol.y / vectol.y;
        } else if (vectol.y < 0) {
            // 負のベクトルの場合は掛け合わせて正となることを防ぐ
            y = (vectol.y / vectol.y) * -1;
        }
        
        vectolKansu = new Vectol(x, y);
        
        return vectolKansu;
    },
    
    // fromVectolとtoVectolから間のxy配列を算出
    calcAidaXyArray: function (fromVectol, toVectol, vectolKansu) {
        var aidaArray = new Array();
        
        // fromVectolからtoVectolにたどり着くまで算出したベクトルを足す
        // iは0から9でなく1から10であることに注意
        for (var i = 1; i < 10; i++) {
            var x = fromVectol.x + (vectolKansu.x * i);
            var y = fromVectol.y + (vectolKansu.y * i);
            
            if (x === toVectol.x && y === toVectol.y) {
                // toVectolにたどり着いたらループを抜ける
                break;
            } else {
                // toVectolにたどり着いていない場合は間の配列にxyを加える
                aidaArray.push(new Vectol(x, y));
            }
        }
        
        return aidaArray;
    },
    
    // 置く箇所に自分の駒があるか判定
    isExistZibunNoKomaAtToVectol: function (toVectol, tugiban) {
        if (tugiban.field[toVectol.x][toVectol.y].html !== NONSTR && tugiban.field[toVectol.x][toVectol.y].muki === this.teban) {
            return true;
        }
        
        return false;
    },
    
    // 置く箇所に敵の駒があるか判定
    isExistAiteNoKomaAtToVectol: function (toVectol, tugiban) {
        if (tugiban.field[toVectol.x][toVectol.y].html !== NONSTR && tugiban.field[toVectol.x][toVectol.y].muki !== this.teban) {
            return true;
        }
        
        return false;
    },
    
    // 敵の駒を駒台に乗せる
    addAiteNoKoma: function (toVectol, tugiban) {
        // 駒台に駒を加える
        this.addKoma(toVectol, tugiban);
        
        // 相手の駒を盤から駒を無くす
        this.removeAitenoKoma(toVectol, tugiban);
    },
    
    
    // 駒台に駒を加える
    addKoma: function (toVectol, tugiban) {
        if (tugiban.field[toVectol.x][toVectol.y].html === "歩" || tugiban.field[toVectol.x][toVectol.y].html === "と") {
            this.addKomadaiArray( new Fu(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "香" || tugiban.field[toVectol.x][toVectol.y].html === "成香") {
            this.addKomadaiArray( new Kyou(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "桂" || tugiban.field[toVectol.x][toVectol.y].html === "成桂") {
            this.addKomadaiArray( new Keima(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "銀" || tugiban.field[toVectol.x][toVectol.y].html === "成銀") {
            this.addKomadaiArray( new Gin(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "角" || tugiban.field[toVectol.x][toVectol.y].html === "馬") {
            this.addKomadaiArray( new Kaku(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "飛" || tugiban.field[toVectol.x][toVectol.y].html === "竜") {
            this.addKomadaiArray( new Hisha(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "金") {
            this.addKomadaiArray( new Kin(this.teban) , tugiban);
        }
        if (tugiban.field[toVectol.x][toVectol.y].html === "王") {
            this.addKomadaiArray( new Ou(this.teban) , tugiban);
        }
    },
    
    // 駒台配列に駒を加える
    addKomadaiArray: function (koma, tugiban) {
        // ソート済みの配列を先頭から検索し空の場所に加える
        // 加える際に駒の向きを変える
        for (var i = 0; i < 40; i++) {
            if (this.teban === SENTE) {
                // 先手の場合、向きを先手に変えて先手の駒台に加える
                if (tugiban.komadaiSente[i].html === NONSTR) {
                    koma.muki = SENTE;
                    tugiban.komadaiSente[i] = koma;
                    
                    return;
                }
            } else {
                // 後手の場合、向きを後手に変えて後手の駒台に加える
                if (tugiban.komadaiGote[i].html === NONSTR) {
                    koma.muki = GOTE;
                    tugiban.komadaiGote[i] = koma;
                    
                    return;
                }
            }
        }
    },
    
    // 駒台配列をシフトする
    shiftKomadaiArray: function () {
        // 隙間がなくなるようシフトする
        if (this.teban === SENTE) {
            for (var i = 0; i < (40 - 1); i++) {
                if (ban.komadaiSente[i].html === NONSTR) {
                    ban.komadaiSente[i] = ban.komadaiSente[i + 1];
                    ban.komadaiSente[i + 1] = new Non();
                }
            }
        } else {
            for (var i = 0; i < (40 - 1); i++) {
                if (ban.komadaiGote[i].html === NONSTR) {
                    ban.komadaiGote[i] = ban.komadaiGote[i + 1];
                    ban.komadaiGote[i + 1] = new Non();
                }
            }
        }
        
        return;
    },
    
    // 相手の駒を盤から駒を無くす
    removeAitenoKoma: function (toVectol, tugiban) {
        tugiban.field[toVectol.x][toVectol.y] = new Non();
    },
    
    // 成れるか判定
    canNaru: function (fromVectol, toVectol, koma) {
        // 成れる個所か判定
        if (!this.isCanNaruPosition(fromVectol, toVectol)) {
            // 成れる箇所でない場合はfalse
            return false;
        }
        
        // 成れる駒か判定
        if (!this.isCanNaruKoma(koma)) {
            // 成れる駒でない場合はfalse
            return false;
        }
        
        return true;
    },
    
    // 成れる箇所か判定
    isCanNaruPosition: function (fromVectol, toVectol) {
        if (this.teban === SENTE) {
            if (fromVectol.y < 3 || toVectol.y < 3) {
                return true;
            }
            
            return false;
        } else {
            if (fromVectol.y > 5 || toVectol.y > 5) {
                return true;
            }
            
            return false;
        }
    },
    
    // 成れる駒か判定
    isCanNaruKoma: function (koma) {
        if (    koma.html === "歩" ||
                koma.html === "香" ||
                koma.html === "桂" ||
                koma.html === "銀" ||
                koma.html === "角" ||
                koma.html === "飛")
        {
            return true;
        }
        
        return false;
    },
    
    
    
    // 駒を置く処理
    doOku: function (toVectol, koma, tugiban) {
        if (koma.html === "歩") {
            tugiban.field[toVectol.x][toVectol.y] = new Fu(this.teban);
        }
        if (koma.html === "香") {
            tugiban.field[toVectol.x][toVectol.y] = new Kyou(this.teban);
        }
        if (koma.html === "成香") {
            tugiban.field[toVectol.x][toVectol.y] = new NariKyou(this.teban);
        }
        if (koma.html === "桂") {
            tugiban.field[toVectol.x][toVectol.y] = new Keima(this.teban);
        }
        if (koma.html === "成桂") {
            tugiban.field[toVectol.x][toVectol.y] = new NariKeima(this.teban);
        }
        if (koma.html === "銀") {
            tugiban.field[toVectol.x][toVectol.y] = new Gin(this.teban);
        }
        if (koma.html === "成銀") {
            tugiban.field[toVectol.x][toVectol.y] = new NariGin(this.teban);
        }
        if (koma.html === "金") {
            tugiban.field[toVectol.x][toVectol.y] = new Kin(this.teban);
        }
        if (koma.html === "角") {
            tugiban.field[toVectol.x][toVectol.y] = new Kaku(this.teban);
        }
        if (koma.html === "飛") {
            tugiban.field[toVectol.x][toVectol.y] = new Hisha(this.teban);
        }
        if (koma.html === "王") {
            tugiban.field[toVectol.x][toVectol.y] = new Ou(this.teban);
        }
        if (koma.html === "馬") {
            tugiban.field[toVectol.x][toVectol.y] = new Uma(this.teban);
        }
        if (koma.html === "竜") {
            tugiban.field[toVectol.x][toVectol.y] = new Ryu(this.teban);
        }
        if (koma.html === "と") {
            tugiban.field[toVectol.x][toVectol.y] = new To(this.teban);
        }
    },
    
    // 動かし元の駒を無くす処理
    doRemoveMotoNoKoma: function (fromVectol, tugiban) {
        tugiban.field[fromVectol.x][fromVectol.y] = new Non();
    },
    
    // 打ち歩詰めを判定
    isUtiFuTume: function(koma, tugiban) {
        // 打った駒が歩かつ詰み(手番側の勝ち)の状態になった場合はtrue
        if (koma.html === "歩" && tugiban.isTumi(this.teban)) {
            return true;
        }
        
        return false;
    }
};