// ------------------------------
// AI ver5.0
// 評価値を利用せず、1手先まで読む。
// 基本的にはシナリオ遷移で指し手を決定する。
// 
// ------------------------------
// コンストラクタ
var Ai5 = function(teban){
    // 手番
    this.teban = teban;
    // 定石
    this.zyoseki = new Zyoseki(this.teban);
    // 戦法
    this.senpou = NONSTR;
    // 囲い
    this.kakoi = NONSTR;
    // util
    this.util = new AiUtil();
};


Ai5.prototype = {
    // 実行する
    execute: function () {
        ban.flashAiHistory();
        
        // 負けが確定している場合、投了する
        var isMake = this.doTouryou(ban);
        if (isMake) {
            return;
        }
        
        var lastTugiban = this.getNextSasite();
        
        // 指す手が存在しない場合は投了する
        if(!lastTugiban.isExistAiHistory()) {
            this.doTouryou(ban);
            
            return;
        }
        
        var fromVectol = lastTugiban.getFirstAiHistoryFromVectol();
        var toVectol = lastTugiban.getFirstAiHistoryToVectol();
        var komadaiNum = lastTugiban.getFirstAiKomadaiNum();
        
        // 人間が指すプロセスと同様に指し手を反映する
        if (komadaiNum === -1) {
            // 盤上の駒を動かす場合
            onClickBan(fromVectol.x, fromVectol.y);
            onClickBan(toVectol.x, toVectol.y);
        } else {
            // 駒台の駒を打つ場合
            onClickKomadai(komadaiNum, this.teban);
            onClickBan(toVectol.x, toVectol.y);
        }
        
        lastTugiban.printAiHistory();
    },
    
    // 次の指し手を取得する
    getNextSasite: function () {
        var result;
        
        // 勝ちの手
        result = this.selectWin(ban);
        if (result !== NONSTR) {
            debbugApendTxt("勝ちの手を採用");
            printMsg(MSG_WIN);
            return result;
        }
        
        // 逃げ
        result = this.selectEscape(ban);
        if (result !== NONSTR) {
            debbugApendTxt("逃げの手を採用");
            printMsg(MSG_ESCAPE);
            return result;
        }
        
        // 詰ませる
        result = this.selectTumaseru(ban, 0);
        if (result !== NONSTR) {
            debbugApendTxt("詰ませるを採用");
            printMsg(MSG_TUMASERU);
            return result;
        }
        
        // 駒得
        result = this.selectKomadoku(ban);
        if (result !== NONSTR) {
            debbugApendTxt("駒得を採用");
            printMsg(MSG_KOMADOKU);
            return result;
        }
        
        // 成り防ぎ(悪手となる場合は除外)
        result = this.selectNariFusegi(ban);
        if (result !== NONSTR) {
            debbugApendTxt("成り防ぎを採用");
            printMsg(MSG_NARIFUSEGI);
            return result;
        }
        
        // 駒攻め(悪手となる場合は除外)
        result = this.selectKomaseme(ban);
        if (result !== NONSTR) {
            debbugApendTxt("駒攻めを採用");
            printMsg(MSG_KOMASEME);
            return result;
        }
        
        // 攻め(悪手となる場合は除外)
        result = this.selectSeme(ban);
        if (result !== NONSTR) {
            debbugApendTxt("攻めを採用");
            printMsg(MSG_SEME);
            return result;
        }
        
        // 突き捨て(悪手となる場合は除外)
        result = this.selectTukisute(ban);
        if (result !== NONSTR) {
            debbugApendTxt("突き捨てを採用");
            printMsg(MSG_TUKISUTE);
            return result;
        }
        
        // 定石(悪手となる場合は除外)
        result = this.selectZyoseki(ban);
        if (result !== NONSTR) {
            debbugApendTxt("定石を採用");
            printMsg(MSG_ZYOSEKI);
            return result;
        }
        
        // 囲う(悪手となる場合は除外)
        result = this.selectKakou(ban);
        if (result !== NONSTR) {
            debbugApendTxt("囲うを採用");
            printMsg(MSG_KAKOU);
            return result;
        }
        
        // 手待ち
        result = this.selectTemati(ban);
        if (result !== NONSTR) {
            debbugApendTxt("手待ちを採用");
            printMsg(MSG_TEMATI);
            return result;
        }
    },
    
    // 相手の成りを防ぐ手を指す
    selectNariFusegi: function (tugiban) {
        // 相手が成れない場合、NONSTR
        if (this.doNaru(tugiban, getAiteTeban(this.teban)) === NONSTR) {
            return NONSTR;
        }
        
        // 1手指す
        var tugibanArray1 = this.getNextTugibanArray(tugiban, this.teban);
        
        // 相手が成れなくなった場合、その継ぎ盤を返却する
        for (var i = 0; i < tugibanArray1.length; i++) {
            if (this.doNaru(tugibanArray1[i], getAiteTeban(this.teban)) === NONSTR) {
                // 指した結果、悪手とならない場合のみ返却
                debbugApendTxt("該当： 成り防ぎ");
                if (!this.isAkuteNariFusegi(tugibanArray1[i])) {
                    return tugibanArray1[i];
                }
            }
        }
        
        return NONSTR;
    },
    
    // 成る
    // ただし、タダで取られる手は除外する
    doNaru: function (tugiban, teban) {
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            var tugibanClone = tugiban.clone();
            
            var beforeNum = this.getNarikomaNum(tugibanClone, teban);
            
            for (var k = 0; k < tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }

                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    var afterNum = this.getNarikomaNum(tugibanClone, teban);
                    
                    // 成り駒が増えていない場合は除外
                    if (beforeNum >= afterNum) {
                        continue;
                    }
                    
                    var isTada = this.util.isTadaLose(tugibanClone, teban, toV);
                    
                    if (!isTada) {
                        return tugibanClone;
                    }
                }
            }
        }
        
        return NONSTR;
    },
    
    // 負けが確定している場合、投了する
    doTouryou: function (tugiban) {
        if (tugiban.isTumi(getAiteTeban(this.teban))) {
            playAudioWin();
            printMsg(MSG_WIN);
            chgCharImg(IMG_LOSE);
            flgGameset = true;
            
            return true;
        }

        return false;
    },
    
    // 勝ちが確定している場合、王を取る
    selectWin: function (tugiban) {
        var tugibanClone = tugiban.clone();
        var humanAi = new HumanAi(this.teban);

        var outeKomaArray = tugiban.getOuteVectolArray(this.teban);
        
        if (outeKomaArray.length === 0) {
            return NONSTR;
        }
        
        var koma = tugibanClone.field[outeKomaArray[0].x][outeKomaArray[0].y];
        var fromV = outeKomaArray[0];

        var toV = tugiban.getOuVectol(getAiteTeban(this.teban));

        var result = humanAi.move(fromV, toV, koma, tugibanClone, true);
        
        if (result) {
            return tugibanClone;
        }

        return NONSTR;
    },
    
    // 王手をかけられている場合、逃げる
    selectEscape: function (tugiban) {
        var outeVArray = tugiban.getOuteVectolArray(getAiteTeban(this.teban));
        if (outeVArray.length === 0) {
            return NONSTR;
        }
        
        var humanAi = new HumanAi(this.teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugiban.field[myVectolArray[i].x][myVectolArray[i].y];
                
                var tugibanClone = tugiban.clone();
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }
                
                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    outeVArray = tugibanClone.getOuteVectolArray(getAiteTeban(this.teban));
                    
                    if (outeVArray.length === 0) {
                        // 王手されなくなる手を返却
                        return tugibanClone;
                    }
                }
            }
        }

        var myKomadaiArray = tugiban.getKomadaiArray(this.teban);
        
        for (var i = 0; i < myKomadaiArray.length; i++) {
            for (var x = 0; x < 8; x++) {
                for (var y = 0; y < 8; y++) {
                    var tugibanClone = tugiban.clone();
                    
                    // 駒台の駒を置く
                    var result = humanAi.put(i, new Vectol(x, y), tugibanClone.field[x][y], tugibanClone);
                    
                    if (result) {
                        outeVArray = tugibanClone.getOuteVectolArray(getAiteTeban(this.teban));
                        
                        if (outeVArray.length === 0) {
                            // 王手されなくなる手を返却
                            return tugibanClone;
                        }
                    }
                }
            }
        }
        
        return NONSTR;
    },
    
    // 詰みを探す
    selectTumaseru: function (tugiban) {
        // 2手先の継ぎ盤配列を取得
        var result2TesakiTugibanArray = this.get2TesakiTugiban(tugiban);
        
        if (result2TesakiTugibanArray.length === 0) {
            // 2手先の継ぎ盤配列が無い場合 = 王手なし = 詰みなし
            return NONSTR;
        } else if (result2TesakiTugibanArray[0].historyAiTeban.length === 1) {
            // 履歴が1手しかない場合 = 詰みを発見
            return result2TesakiTugibanArray[0];
        }
        
        return NONSTR;
    },
    
    // 2手先の継ぎ盤を取得
    get2TesakiTugiban: function (tugiban) {
        var canEscapeOuteArray = new Array();
        
        var humanAi = new HumanAi(this.teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugiban.field[myVectolArray[i].x][myVectolArray[i].y];
                
                var tugibanClone = tugiban.clone();
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }
                
                // 盤上の駒を動かす
                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    // 王手をかけているか
                    var outeArray = tugibanClone.getOuteArray(this.teban);
                    if (outeArray.length === 0) {
                        continue;
                    }
                    
                    // 相手に王手されている場合は除外
                    var outeZibunArray = tugibanClone.getOuteArray(getAiteTeban(this.teban));
                    if (outeZibunArray.length !== 0) {
                        continue;
                    }

                    var doEscapeOuteTugibanArray = this.getDoEscapeOuteTugibanArray(tugibanClone, getAiteTeban(this.teban));

                    if (doEscapeOuteTugibanArray.length === 0) {
                        // 詰みを発見
                        var temTugibanCloneArray = new Array();
                        temTugibanCloneArray.push(tugibanClone);
                        return temTugibanCloneArray;
                    }
                    
                    canEscapeOuteArray.concat(doEscapeOuteTugibanArray);
                    // TODO 4手先以上の場合は再度考える必要あり
                    
                }
            }
        }

        var myKomadaiArray = tugiban.getKomadaiArray(this.teban);
        
        for (var i = 0; i < myKomadaiArray.length; i++) {
            for (var x = 0; x < 8; x++) {
                for (var y = 0; y < 8; y++) {
                    // 継ぎ盤の初期化
                    var tugibanClone = tugiban.clone();
                    
                    // 駒台の駒を置く
                    var result = humanAi.put(i, new Vectol(x, y), myKomadaiArray[i], tugibanClone);
                    
                    if (result) {
                        // 王手をかけているか
                        var outeArray = tugibanClone.getOuteArray(this.teban);

                        if (outeArray.length === 0) {
                            continue;
                        }

                        var doEscapeOuteTugibanArray = this.getDoEscapeOuteTugibanArray(tugibanClone, getAiteTeban(this.teban));
                        
                        if (doEscapeOuteTugibanArray.length === 0) {
                            // 詰みを発見
                            var temTugibanCloneArray = new Array();
                            temTugibanCloneArray.push(tugibanClone);
                            return temTugibanCloneArray;
                        }
                        
                        canEscapeOuteArray.concat(doEscapeOuteTugibanArray);
                        // TODO 4手先以上の場合は再度考える必要あり
                        
                    }
                }
            }
        }

        return canEscapeOuteArray;
    },
    
    // 引数の手番が相手の王手から逃げられる継ぎ盤の配列を取得
    getDoEscapeOuteTugibanArray: function (tugiban, teban) {
        var returnTugibanArray = new Array();
        
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var tugibanClone = tugiban.clone();
                
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }

                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    // 王手がかかっているか
                    var outeArray = tugibanClone.getOuteArray(getAiteTeban(teban));

                    if (outeArray.length !== 0) {
                        continue;
                    }
                    
                    // 逃げられた
                    returnTugibanArray.push(tugibanClone);
                }
            }
        }
        
        return returnTugibanArray;
    },
    
    // 引数の手番の成り駒の数を取得する
    getNarikomaNum: function (tugiban, teban) {
        var num = 0;
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            var koma = tugiban.field[myVectolArray[i].x][myVectolArray[i].y];
            
            if (koma.html === "竜" || koma.html === "馬" || koma.html === "成香" || koma.html === "成桂" || koma.html === "成銀" || koma.html === "と") {
                num++;
            }
        }
        
        return num;
    },
    
    // 駒得となる手を取得する
    // 同時に駒損も防ぐ
    selectKomadoku: function (tugiban) {
        var tugibanClone = tugiban.clone();
        
        // 何もしなかった場合
        // 最悪のケースの駒損のスコアを保存
        var tmpTugibanArray1 = this.getNextTugibanArray(tugibanClone, getAiteTeban(this.teban));
        var beforeScoreTugiban = this.getMaxScoreTugiban(tmpTugibanArray1, getAiteTeban(this.teban));
        var beforeScore = beforeScoreTugiban.getTargetTebanScoreSum(getAiteTeban(this.teban));
        
        // 自分が何らかの手を指す場合
        var tugibanArray1 = this.getNextTugibanArray(tugibanClone, this.teban);
        var tugibanArray2 = new Array();
        for (var i = 0; i < tugibanArray1.length; i++) {
            // 相手は最大の駒得の手を指すと仮定
            var tmpArray = this.getNextTugibanArray(tugibanArray1[i], getAiteTeban(this.teban));
            var maxScoreTugibanAite = this.getMaxScoreTugiban(tmpArray, getAiteTeban(this.teban));
            tugibanArray2.push(maxScoreTugibanAite);
        }
        // 自分にとって最高の手を取得
        var afterScoreTugiban = this.getMaxScoreTugiban(tugibanArray2, this.teban);
        var afterScore = afterScoreTugiban.getTargetTebanScoreSum(getAiteTeban(this.teban));
        
        // スコアが改善されない場合はNONSTR
        if (beforeScore <= afterScore) {
            return NONSTR;
        }
        
        return afterScoreTugiban;
    },
    
    // 次の継ぎ盤の配列を取得する
    // ただし駒を取る場合、紐付きの駒にはより価値が高い駒だけを対象にする
    getNextTugibanArray: function (tugiban, teban) {
        var nextTugibanArray = new Array();
        
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                // 試しに動かす継ぎ盤を作成
                var tugibanClone = tugiban.clone();

                var fromVectol = new Vectol(myVectolArray[i].x, myVectolArray[i].y);
                var toVectol = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];

                // 駒がない場合は除外
                if (koma.html === NONSTR) {
                    continue;
                }
                
                if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                    continue;
                }
                
                // toVectolが相手の駒かつ紐付きかつ価値が低い場合は除外
                if (    tugibanClone.field[toVectol.x][toVectol.y].muki === getAiteTeban(teban) &&
                        this.util.isAbleHimoToTarget(tugibanClone, toVectol, getAiteTeban(teban)) &&
                        koma.scoreMotigoma > tugibanClone.field[toVectol.x][toVectol.y].scoreMotigoma) {
                    continue;
                }
                
                // 疑似的に2回目クリックしたと仮定
                var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

                // 動かすことができた場合
                if (result) {
                    nextTugibanArray.push(tugibanClone);
                }
            }
        }
        
        return nextTugibanArray;
    },
    
    // 最大スコアの継ぎ盤を取得する
    getMaxScoreTugiban: function (tugibanArray, teban) {
        var maxScore = -1;
        var maxScoreSoezi = -1;
        
        for (var i = 0; i < tugibanArray.length; i++) {
            var tmpScore = tugibanArray[i].getTargetTebanScoreSum(teban);
            
            if (tmpScore > maxScore) {
                maxScore = tmpScore;
                maxScoreSoezi = i;
            }
        }
        
        return tugibanArray[maxScoreSoezi];
    },
    
    // スコアに差分があるか判定
    isSameScore: function (tugibanArray, teban) {
        var maxScore = -1;
        
        // 比較するものが無い場合はfalse
        if (tugibanArray.length === 0) {
            return false;
        }
        
        for (var i = 0; i < tugibanArray.length; i++) {
            var tmpScore = tugibanArray[i].getTargetTebanScoreSum(teban);
            
            // 初回の値をベースに設定
            if (i === 0) {
                maxScore = tmpScore;
            } else {
                if (maxScore !== tmpScore) {
                    return false;
                }
            }
        }
        
        return true;
    },
    
    // 定石を指す
    selectZyoseki: function (tugiban) {
        var resultTugiban;

        // 指し手が20手目以降の場合はNONSTR
        if (tugiban.kifuTeban.length >= 20) {
            return NONSTR;
        }

        this.selectMySenpou(tugiban);

        resultTugiban = this.zyoseki.executeSenpou(tugiban, this.teban, this.senpou);
        
        if (resultTugiban !== NONSTR) {
            // 指した結果、悪手とならない場合のみ返却
            debbugApendTxt("該当： 定石");
            if (!this.isAkute(resultTugiban)) {
                return resultTugiban;
            }
        }
        
        return NONSTR;
    },
    
    // 戦法を決める
    // 囲いが美濃囲い = 四間飛車、囲いが美濃囲い以外 = 棒銀
    selectMySenpou: function (tugiban) {
        var isExistKakuFlg = false;
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        for (var i = 0; i < myVectolArray.length; i++) {
            if (tugiban.field[myVectolArray[i].x][myVectolArray[i].y].html === "角") {
                isExistKakuFlg = true;
                break;
            }
        }
        
        if (!isExistKakuFlg) {
            this.senpou = SENPOU_BOUGIN;
        } else {
            this.senpou = SENPOU_SIKENBISHA;
        }
        
        debbugApendTxt("戦法決定： " + this.senpou);
        return;
    },
    
    // 手待ちを指す
    selectTemati: function (tugiban) {
        // 歩を動かして手待ち
        var result = this.selectTematiFu(tugiban);
        
        if (result !== NONSTR) {
            return result;
        }
        
        // 負けないために指せる手を指す
        result = this.selectTematiLast(tugiban);
        
        if (result !== NONSTR) {
            return result;
        }
        
        return NONSTR;
    },
    
    // 手待ちを指す
    // 歩を動かして手待ちする
    selectTematiFu: function (tugiban) {
        // 端に近い歩を伸ばす
        // ぶつかる場合は指さない
        var humanAi = new HumanAi(this.teban);
        
        var array = new Array();
        array.push(0);
        array.push(8);
        array.push(1);
        array.push(7);
        array.push(2);
        array.push(6);
        array.push(3);
        array.push(5);
        array.push(4);
        
        for (var idx = 0;idx < 9;idx++) {
            for (var j = 0;j < 7;j++) {
                var i = array[idx];
                
                if (tugiban.field[i][j].muki === this.teban) {
                    for (var k = 0;k < tugiban.field[i][j].vectol.length;k++) {
                        // 試しに動かす継ぎ盤を作成
                        var tugibanClone = tugiban.clone();
                        
                        var fromVectol = new Vectol(i, j);
                        var toVectol = new Vectol(i + tugibanClone.field[i][j].vectol[k].x, j + tugibanClone.field[i][j].vectol[k].y);
                        var koma = tugibanClone.field[i][j];
                        
                        // 歩でない場合は何もしない
                        if (koma.html !== "歩") {
                            continue;
                        }
                        
                        if ( !( 0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8 ) ) {
                            continue;
                        }
                        
                        // 歩の先に相手の駒がある場合は何もしない
                        if (tugibanClone.field[toVectol.x][toVectol.y + 1].muki === getAiteTeban(this.teban)) {
                            continue;
                        }
                        
                        var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);
                        
                        if (result) {
                            return tugibanClone;
                        }
                    }
                }
            }
        }
        
        return NONSTR;
    },
    
    // 手待ちを指す
    // 動かせる駒を動かす
    selectTematiLast: function (tugiban) {
        var humanAi = new HumanAi(this.teban);

        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);

        for (var i = 0; i < myVectolArray.length; i++) {
            var tugibanClone = tugiban.clone();

            for (var k = 0; k < tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }

                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    return tugibanClone;
                }
            }
        }

        var myKomadaiArray = tugiban.getKomadaiArray(this.teban);

        for (var i = 0; i < myKomadaiArray.length; i++) {
            var tugibanClone = tugiban.clone();

            for (var x = 0; x < 8; x++) {
                for (var y = 0; y < 8; y++) {
                    var result = humanAi.put(i, new Vectol(x, y), tugibanClone.field[x][y], tugibanClone);

                    if (result) {
                        return tugibanClone;
                    }
                }
            }
        }

        return NONSTR;
    },
    
    // 攻める手を指す
    // 成りを狙う、または大ゴマを打ち込む手を狙う
    selectSeme: function (tugiban) {
        var humanAi = new HumanAi(this.teban);
        
        // 成りを狙う
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            var tugibanClone = tugiban.clone();
            
            var beforeNum = this.getNarikomaNum(tugibanClone, this.teban);
            
            for (var k = 0; k < tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                var fromV = myVectolArray[i];
                var toV = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[k].y);
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
                
                if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                    continue;
                }

                var result = humanAi.move(fromV, toV, koma, tugibanClone, true);

                if (result) {
                    var afterNum = this.getNarikomaNum(tugibanClone, this.teban);
                    
                    // 成り駒が増えていない場合は除外
                    if (beforeNum >= afterNum) {
                        continue;
                    }
                    
                    var isTada = this.util.isTadaLose(tugibanClone, this.teban, toV);
                    
                    if (!isTada) {
                        // 指した結果、悪手とならない場合のみ返却
                        debbugApendTxt("該当： 攻め");
                        if (!this.isAkute(tugibanClone)) {
                            return tugibanClone;
                        }
                    }
                }
            }
        }
        
        // 大駒の打ち込みを狙う
        var myKomadaiArray = tugiban.getKomadaiArray(this.teban);
        
        for (var i = 0; i < myKomadaiArray.length; i++) {
            var tugibanClone = tugiban.clone();
            
            if (myKomadaiArray[i].html === "飛" || myKomadaiArray[i].html === "角") {
                for (var x = 0; x < 8; x++) {
                    for (var y = 6; y < 8; y++) {
                        var result = humanAi.put(i, new Vectol(x, y), tugibanClone.field[x][y], tugibanClone);
                        
                        if (result) {
                            var isTada = this.util.isTadaLose(tugibanClone, this.teban, new Vectol(x, y));
                            
                            if (!isTada) {
                                // 指した結果、悪手とならない場合のみ返却
                                debbugApendTxt("該当： 攻め");
                                if (!this.isAkute(tugibanClone)) {
                                    return tugibanClone;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return NONSTR;
    },
    
    // 突き捨ての手を指す
    selectTukisute: function (tugiban) {
        var humanAi = new HumanAi(this.teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            var tugibanClone = tugiban.clone();
            
            var fromV = myVectolArray[i];
            var toV = new Vectol(myVectolArray[i].x + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[0].x, myVectolArray[i].y + tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y].vectol[0].y);
            var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
            
            if (koma.html !== "歩") {
                continue;
            }
                
            var canMove = humanAi.move(fromV, toV, koma, tugibanClone, true);
            if (!canMove) {
                continue;
            }

            var isHimotuki = this.util.isAbleHimoToTarget(tugibanClone, toV, this.teban);
            if (!isHimotuki) {
                continue;
            }
            
            // 紐付きの歩を、相手が取れる、かつ取った後の相手の駒に、相手の紐がついていないかを判定
            var canMoveAfterHimoNon = this.util.moveAfterHimoNon(tugibanClone, toV, getAiteTeban(this.teban));
            if (!canMoveAfterHimoNon) {
                continue;
            }
            
            return tugibanClone;
        }
        
        return NONSTR;
    },
    
    // 囲うを指す
    selectKakou: function (tugiban) {
        var resultTugiban;
        
        // 指し手が20手目以降の場合は無条件にNONSTR
        if (tugiban.kifuTeban.length >= 20) {
            return NONSTR;
        }

        this.selectMyKakoi(tugiban);

        resultTugiban = this.zyoseki.executeKakou(tugiban, this.teban, this.kakoi);
        
        if (resultTugiban !== NONSTR) {
            // 指した結果、悪手とならない場合のみ返却
            debbugApendTxt("該当： 囲う");
            if (!this.isAkute(resultTugiban)) {
                return resultTugiban;
            }
        }
        
        return NONSTR;
    },
    
    // 囲いを決める
    selectMyKakoi: function (tugiban) {
        if (this.kakoi !== NONSTR) {
            return;
        }
        
        // 角が盤上に存在しない場合 = カニ囲い
        var isExistKakuBanzyou = this.isExistKakuBanzyou(tugiban, this.teban);
        
        if (!isExistKakuBanzyou) {
            this.kakoi = KAKOI_KANIGAKOI;
            debbugApendTxt("囲い決定： " + this.kakoi);
            return;
        }
        
        // 自分と相手の飛車の位置で囲いを決める
        var isHisyaLeftPositionMine = this.isHisyaLeftPosition(tugiban, this.teban);
        var isHisyaLeftPositionAite = this.isHisyaLeftPosition(tugiban, getAiteTeban(this.teban));
        
        if (!isHisyaLeftPositionMine) {
            this.kakoi = KAKOI_MINOGAKOI;
        } else {
            if (isHisyaLeftPositionAite) {
                this.kakoi = KAKOI_FUNAGAKOI;
            } else {
                this.kakoi = KAKOI_KANIGAKOI;
            }
        }
        
        debbugApendTxt("囲い決定： " + this.kakoi);
        return;
    },
    
    // 盤上に角が存在するか判定
    isExistKakuBanzyou: function (tugiban, teban) {
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        for (var i = 0; i < myVectolArray.length; i++) {
            if (tugiban.field[myVectolArray[i].x][myVectolArray[i].y].html === "角") {
                return true;
            }
        }
        
        return false;
    },
    
    // 飛車が左の位置にあるか判定
    isHisyaLeftPosition: function (tugiban, teban) {
        var myteVectolArray = tugiban.getTargetTebanVectolArray(teban);
        for (var i = 0; i < myteVectolArray.length; i++) {
            if (tugiban.field[myteVectolArray[i].x][myteVectolArray[i].y].html === "飛") {  
                if (myteVectolArray[i].x >= 4) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // 駒攻めを指す
    // 相手の歩以外の駒に利きを利かす
    // ただし、利きを利かした駒がタダで取られる場合は除外する
    selectKomaseme: function (tugiban) {
        var humanAi = new HumanAi(this.teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(this.teban);
        var aiteVectolArray = tugiban.getTargetTebanVectolArray(getAiteTeban(this.teban));
        
        // 盤上
        for (var i = 0; i < aiteVectolArray.length; i++) {
            if (tugiban.field[aiteVectolArray[i].x][aiteVectolArray[i].y].html === "歩") {
                continue;
            }
            
            for (var j = 0; j < myVectolArray.length; j++) {
                var tugibanClone = tugiban.clone();
                
                if (tugibanClone.field[myVectolArray[j].x][myVectolArray[j].y].html === "王") {
                    continue;
                }
                
                for (var k = 0; k < tugibanClone.field[myVectolArray[j].x][myVectolArray[j].y].vectol.length; k++) {
                    var fromV = myVectolArray[j];
                    var toV = new Vectol(myVectolArray[j].x + tugibanClone.field[myVectolArray[j].x][myVectolArray[j].y].vectol[k].x, tugibanClone.field[myVectolArray[j].x][myVectolArray[j].y].vectol[k].y);
                    var koma = tugibanClone.field[myVectolArray[j].x][myVectolArray[j].y];
                    
                    if (!(0 <= toV.x && toV.x <= 8 && 0 <= toV.y && toV.y <= 8)) {
                        continue;
                    }
                    
                    var result = humanAi.move(fromV, toV, koma, tugibanClone, true);
                    
                    if (result) {
                        var isKiki = this.util.isKikiTargetOnlyOne(tugibanClone, aiteVectolArray[i], this.teban, toV);
                        var isTada = this.util.isTadaLose(tugibanClone, this.teban, toV);
                        
                        // 相手の大駒に利きがあり、かつタダで取られない場合
                        if (isKiki && !isTada) {
                            // 指した結果、悪手とならない場合のみ返却
                            debbugApendTxt("該当： 駒攻め");
                            if (!this.isAkute(tugibanClone)) {
                                return tugibanClone;
                            }
                        }
                    }
                }
            }
            
            // 駒台
            var komadaiArray = tugiban.getKomadaiArray(this.teban);
            for (var i2 = 0; i2 < komadaiArray.length; i2++) {
                for (var j2 = 0; j2 < 9; j2++) {
                    for (var k2 = 0; k2 < 9; k2++) {
                        var koma = komadaiArray[i2];

                        // 駒がない場合は何もしない
                        if (koma.html === NONSTR) {
                            continue;
                        }

                        var tugibanClone = tugiban.clone();

                        var toVectol = new Vectol(j2, k2);

                        var result = humanAi.put(i2, toVectol, koma, tugibanClone);
                        
                        if (result) {
                            var isKiki = this.util.isKikiTargetOnlyOne(tugibanClone, aiteVectolArray[i], teban, toVectol);
                            var isTada = this.util.isTadaLose(tugibanClone, this.teban, toVectol);
                            
                            // 相手の大駒に利きがあり、かつタダで取られない場合
                            if (isKiki && !isTada) {
                                // 指した結果、悪手とならない場合のみ返却
                                debbugApendTxt("該当： 駒攻め");
                                if (!this.isAkute(tugibanClone)) {
                                    return tugibanClone;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return NONSTR;
    },
    
    // 指した結果悪手となるか判定
    isAkute: function (tugiban) {
        var result;
        
        // 逃げ
        result = this.selectEscape(tugiban);
        if (result !== NONSTR) {
            debbugApendTxt("逃げの手に該当 = 悪手");
            return true;
        }
        
        return false;
    },
    
    // 指した結果悪手となるか判定(成り防ぎ専用)
    isAkuteNariFusegi: function (tugiban) {
        var result;
        
        // 逃げ
        result = this.selectEscape(tugiban);
        if (result !== NONSTR) {
            debbugApendTxt("逃げの手に該当 = 悪手");
            return true;
        }
        
        // 駒得
        result = this.selectKomadoku(tugiban);
        if (result !== NONSTR) {
            debbugApendTxt("駒得の手に該当 = 悪手");
            return true;
        }
        
        return false;
    }
};
