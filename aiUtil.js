// ------------------------------
// AiUtil
// AIで汎用的に使用するメソッドを定義。
// 
// ------------------------------
// コンストラクタ
var AiUtil = function(){
};


AiUtil.prototype = {
    // ターゲットに利きがあるか判定
    isKikiTarget: function(tugiban, targetVectol, teban) {
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                // 試しに動かす継ぎ盤を作成
                var tugibanClone = tugiban.clone();

                var fromVectol = new Vectol(myVectolArray[i].x, myVectolArray[i].y);
                var toVectol = targetVectol;
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];

                // 駒がない場合は何もしない
                if (koma.html === NONSTR) {
                    continue;
                }

                // toVectolが盤外となっていた場合は何もしない
                if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                    continue;
                }

                // 疑似的に2回目クリックしたと仮定
                var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

                // 動かすことができた場合
                if (result) {
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // 指定された駒によってターゲットに利きがあるか判定
    isKikiTargetOnlyOne: function(tugiban, targetVectol, teban, fromVectol) {
        var humanAi = new HumanAi(teban);
        
        var i = fromVectol.x;
        var j = fromVectol.y;
        
        if (tugiban.field[i][j].muki === teban) {
            for (var k = 0; k < tugiban.field[i][j].vectol.length; k++) {
                // 試しに動かす継ぎ盤を作成
                var tugibanClone = tugiban.clone();

                var fromVectol = new Vectol(i, j);
                var toVectol = targetVectol;
                var koma = tugibanClone.field[i][j];

                // 駒がない場合は何もしない
                if (koma.html === NONSTR) {
                    continue;
                }

                // toVectolが盤外となっていた場合は何もしない
                if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                    continue;
                }

                // 疑似的に2回目クリックしたと仮定
                var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

                // 動かすことができた場合
                if (result) {
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // ターゲットが逃げられるか判定
    canEscapeTarget: function(tugiban, targetVectol, teban) {
        var humanAi = new HumanAi(teban);
        
        var i = targetVectol.x;
        var j = targetVectol.y;
        
        for (var k = 0; k < tugiban.field[i][j].vectol.length; k++) {
            // 試しに動かす継ぎ盤を作成
            var tugibanClone = tugiban.clone();

            var fromVectol = new Vectol(i, j);
            var toVectol = new Vectol(i + tugibanClone.field[i][j].vectol[k].x, j + tugibanClone.field[i][j].vectol[k].y);
            var koma = tugibanClone.field[i][j];

            // 駒がない場合は何もしない
            if (koma.html === NONSTR) {
                continue;
            }

            // toVectolが盤外となっていた場合は何もしない
            if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                continue;
            }

            // 疑似的に2回目クリックしたと仮定
            var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

            // 動かすことができた場合
            if (result) {
                // ターゲットに利きがあるか判定
                var result2 = this.isKikiTarget(tugibanClone, targetVectol, getAiteTeban(teban));
                
                if (!result2) {
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // 最終的に駒損となるか判定
    // AIが最後に指した駒を取られるか判定する
    // 取られる場合は取った駒と取られた駒の価値を比較する
    isKomazonLastTurn: function(tugiban, teban, toVectol) {
        var humanAi = new HumanAi(getAiteTeban(teban));
        
        // 自分が取った駒を取得
        var getKomaMine = ban.field[toVectol.x][toVectol.y];
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        // 相手が自分が最後に指した手を取れるか判定
        for (var i = 0; i < myVectolArray.length; i++) {
            // 試しに動かす継ぎ盤を作成
            var tugibanClone = tugiban.clone();
            var fromVectol = new Vectol(myVectolArray[i].x, myVectolArray[i].y);
            var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
            // 駒がない場合は何もしない
            if (koma.html === NONSTR) {
                continue;
            }

            // toVectolが盤外となっていた場合は何もしない
            if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                continue;
            }

            // 疑似的に2回目クリックしたと仮定
            var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

            // 動かすことができた場合
            if (result) {
                // 相手が取った駒を取得
                var getKomaAite = tugiban.field[toVectol.x][toVectol.y];

                // 自分が取った駒と取られた駒の価値を比較
                if (getKomaMine.scoreMotigoma < getKomaAite.scoreMotigoma) {
                    // 相手が駒得する場合はtrue
                    return true;
                }
            }
        }
        
        return false;
    },
    
    // タダで損となるか判定
    isTadaLose: function(tugiban, teban, target) {
        // targetの駒が相手に取られるか判定
        var result2 = this.isKikiTarget(tugiban, target, getAiteTeban(teban));
        
        if (!result2) {
            // 相手に取られない場合はfalse
            return false;
        }
        
        // targetの駒に紐がついているか判定
        var result = this.isAbleHimoToTarget(tugiban, target, teban);
        
        if (result) {
            // 紐がついている場合かつ、取られる駒 <= 取る駒の場合のみfalse
            var minAiteScore = this.getIsKikiTargetMinScore(tugiban, target, getAiteTeban(teban));
            var myScore = tugiban.field[target.x][target.y].scoreMotigoma;
            
            if (myScore <= minAiteScore) {
                return false;
            }
        }
        
        return true;
    },
    
    // 駒がぶつかっているか判定
    isButukaru: function(tugiban) {
        for (var i = 0;i < 9;i++) {
            for (var j = 0;j < 8;j++) {
                // 自分の駒の前に相手の駒が存在するか判定
                if (tugiban.field[i][j].muki === this.teban) {
                    if (tugiban.field[i][j + 1].muki === getAiteTeban(this.teban)) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    },
    
    // ターゲットに紐がついているか判定
    isAbleHimoToTarget: function(tugiban, targetVectol, teban) {
        var humanAi = new HumanAi(teban);
        var tugibanClone = tugiban.clone();
        
        var myVectolArray = tugibanClone.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            // ターゲット自身の駒は除外
            if (myVectolArray[i].x === targetVectol.x && myVectolArray[i].y === targetVectol.y) {
                continue;
            }

            var fromVectol = new Vectol(myVectolArray[i].x, myVectolArray[i].y);
            var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];

            // 自分の上に駒を乗せる
            var result = humanAi.moveToMyKoma(fromVectol, targetVectol, koma, tugibanClone, true);

            if (result) {
                return true;
            }
        }
        
        return false;
    },
    
    // ターゲットに利きがあるか判定
    getIsKikiTargetMinScore: function(tugiban, targetVectol, teban) {
        var minScore = 999999;
        
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            for (var k = 0; k < tugiban.field[myVectolArray[i].x][myVectolArray[i].y].vectol.length; k++) {
                // 試しに動かす継ぎ盤を作成
                var tugibanClone = tugiban.clone();

                var fromVectol = new Vectol(myVectolArray[i].x, myVectolArray[i].y);
                var toVectol = targetVectol;
                var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];

                // 駒がない場合は何もしない
                if (koma.html === NONSTR) {
                    continue;
                }

                // toVectolが盤外となっていた場合は何もしない
                if (!(0 <= toVectol.x && toVectol.x <= 8 && 0 <= toVectol.y && toVectol.y <= 8)) {
                    continue;
                }

                // 疑似的に2回目クリックしたと仮定
                var result = humanAi.move(fromVectol, toVectol, koma, tugibanClone, true);

                // 動かすことができた場合
                if (result) {
                    if (minScore > koma.scoreMotigoma) {
                        minScore = koma.scoreMotigoma;
                    }
                }
            }
        }
        
        return minScore;
    },
    
    // 取った後の駒に紐がついていない状態となる場合、ターゲットの駒をとる
    moveAfterHimoNon: function(tugiban, targetVectol, teban) {
        var humanAi = new HumanAi(teban);
        
        var myVectolArray = tugiban.getTargetTebanVectolArray(teban);
        
        for (var i = 0; i < myVectolArray.length; i++) {
            var tugibanClone = tugiban.clone();
            
            var fromV = myVectolArray[i];
            var toV = new Vectol(targetVectol.x, targetVectol.y);
            var koma = tugibanClone.field[myVectolArray[i].x][myVectolArray[i].y];
                
            var canMove = humanAi.move(fromV, toV, koma, tugibanClone, true);
            if (!canMove) {
                continue;
            }

            var isHimo = this.isAbleHimoToTarget(tugibanClone, toV, teban);
            if (isHimo) {
                continue;
            }
            
            tugiban = tugibanClone;
            return true;
        }
        
        return false;
    }
};
