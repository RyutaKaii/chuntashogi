// ------------------------------
// Human
// 人間の動作を定義。
// 
// ------------------------------
// コンストラクタ
var Human = function(teban){
    // 手番
    this.teban = teban;
    // util
    this.util = new HumanUtil(this.teban);
};

Human.prototype = {
    // 駒台の駒を置く
    put: function(komadaiNum, toVectol, koma, tugiban) {
        // 盤をバックアップ
        var banBk = new Ban();
        banBk.setBan(tugiban);
        
        // 自分の駒か判定
        if (koma.muki !== this.teban) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 置く箇所に自分の駒があるか判定
        if (this.util.isExistZibunNoKomaAtToVectol(toVectol, tugiban)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 置く箇所に敵の駒があるか判定
        if (this.util.isExistAiteNoKomaAtToVectol(toVectol, tugiban)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 置ける場所か判定
        if (!this.util.isPutPosition(toVectol, koma)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 二歩を判定
        if (this.util.isNifu(toVectol, koma, tugiban)) {
            // 動かせない時のエラー処理
            this.doCannotMove("二歩です。");
            return false;
        }
        
        // 駒台の駒を無くす処理
        this.util.removeKomadai(komadaiNum, tugiban);
        
        // 駒を置く処理
        this.util.doOku(toVectol, koma, tugiban);
        
        // 打ち歩詰めを判定
        // 打ち歩詰めとなる場合は局面を元に戻す
        if (this.util.isUtiFuTume(koma, tugiban)) {
            // 動かせない時のエラー処理
            this.doCannotMove("打ち歩詰めとなるため置けません。");
            
            // 盤をバックアップした状態に戻す
            tugiban.setBan(banBk);
            
            return false;
        }
        
        return true;
    },
    
    // 駒を動かす
    move: function (fromVectol, toVectol, koma, tugiban) {
        // 駒を動かせるか判定
        if (!this.util.canMove(fromVectol, toVectol, koma)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 間に駒があるか判定
        if (this.util.isExistAidaKoma(fromVectol, toVectol, koma, tugiban)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 置く箇所に自分の駒があるか判定
        if (this.util.isExistZibunNoKomaAtToVectol(toVectol, tugiban)) {
            // 動かせない時のエラー処理
            return false;
        }
        
        // 置く箇所に敵の駒があるか判定
        if (this.util.isExistAiteNoKomaAtToVectol(toVectol, tugiban)) {
            // 敵の駒を駒台に乗せる
            this.util.addAiteNoKoma(toVectol, tugiban);
        }
        
        // 成れるか判定
        if (this.util.canNaru(fromVectol, toVectol, koma, tugiban)) {
            // 成れる駒の場合の処理 成らない場合は駒を置く処理を実施
            this.doNaru(toVectol, koma, tugiban);
        } else {
            // 成れない駒の場合の処理
            // 駒を置く
            this.util.doOku(toVectol, koma, tugiban);
        }
        
        // 動かし元の駒を無くす処理
        this.util.doRemoveMotoNoKoma(fromVectol, tugiban);
        
        return true;
    },
    
    // 動かせない時のエラー処理
    doCannotMove: function (txt) {
        alert(txt);
    },
    
    // 成る処理
    doNaru: function (toVectol, koma, tugiban) {
        if (window.confirm('成りますか？')) {
            // 確認ダイアログでYesを選択したときの処理
            if (koma.html === "歩") {
                tugiban.field[toVectol.x][toVectol.y] = new To(this.teban);
            }
            if (koma.html === "香") {
                tugiban.field[toVectol.x][toVectol.y] = new NariKyou(this.teban);
            }
            if (koma.html === "桂") {
                tugiban.field[toVectol.x][toVectol.y] = new NariKeima(this.teban);
            }
            if (koma.html === "銀") {
                tugiban.field[toVectol.x][toVectol.y] = new NariGin(this.teban);
            }
            if (koma.html === "角") {
                tugiban.field[toVectol.x][toVectol.y] = new Uma(this.teban);
            }
            if (koma.html === "飛") {
                tugiban.field[toVectol.x][toVectol.y] = new Ryu(this.teban);
            }
        } else {
            // 確認ダイアログでNoを選択したときの処理
            // 駒を置く
            this.util.doOku(toVectol, koma, tugiban);
        }
    }
};