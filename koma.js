// ------------------------------
// 駒 歩
// ------------------------------
// コンストラクタ
var Fu = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(1);
    this.vectol[0] = new Vectol(0, 1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(0, 1 * -1);
    }
    
    // html
    this.html = "歩";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_fu.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_fu.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 100;
    
    // 持ち駒点数
    this.scoreMotigoma = 1000;
};

// ------------------------------
// 駒 香
// ------------------------------
// コンストラクタ
var Kyou = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(9);
    for (i = 0; i < 9; i++) {
        this.vectol[i] = new Vectol(0, i);
    }
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        for (i = 0; i < 9; i++) {
            this.vectol[i] = new Vectol(0, i * -1);
        }
    }
    
    // html
    this.html = "香";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_kyou.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_kyou.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 400;
    
    // 持ち駒点数
    this.scoreMotigoma = 1100;
};

// ------------------------------
// 駒 成香
// ------------------------------
// コンストラクタ
var NariKyou = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(6);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
    }
    
    // html
    this.html = "成香";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_narikyou.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_narikyou.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1100;
};

// ------------------------------
// 駒 桂馬
// ------------------------------
// コンストラクタ
var Keima = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(2);
    this.vectol[0] = new Vectol(-1, 2);
    this.vectol[1] = new Vectol(1, 2);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 2 * -1);
        this.vectol[1] = new Vectol(1, 2 * -1);
    }
    
    // html
    this.html = "桂";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_keima.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_keima.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 500;
    
    // 持ち駒点数
    this.scoreMotigoma = 1200;
};

// ------------------------------
// 駒 成桂
// ------------------------------
// コンストラクタ
var NariKeima = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(6);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
    }
    
    // html
    this.html = "成桂";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_narikei.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_narikei.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1200;
};

// ------------------------------
// 駒 銀
// ------------------------------
// コンストラクタ
var Gin = function(teban){
    // 向き
    this.muki = teban;

    // ベクトル
    this.vectol = new Array(5);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, -1);
    this.vectol[4] = new Vectol(1, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, -1 * -1);
        this.vectol[4] = new Vectol(1, -1 * -1);
    }
    
    // html
    this.html = "銀";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_gin.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_gin.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 700;
    
    // 持ち駒点数
    this.scoreMotigoma = 1300;
};

// ------------------------------
// 駒 成銀
// ------------------------------
// コンストラクタ
var NariGin = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(6);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
    }
    
    // html
    this.html = "成銀";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_narigin.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_narigin.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1300;
};

// ------------------------------
// 駒 金
// ------------------------------
// コンストラクタ
var Kin = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(6);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
    }
    
    // html
    this.html = "金";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_kin.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_kin.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1400;
};

// ------------------------------
// 駒 角
// ------------------------------
// コンストラクタ
var Kaku = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(36);
    for (i = 0; i < 9; i++) {
        this.vectol[i] = new Vectol( (i * -1), i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 9] = new Vectol(i, i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 18] = new Vectol( (i * -1), (i * -1));
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 27] = new Vectol(i, (i * -1));
    }
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        for (i = 0; i < 9; i++) {
            this.vectol[i] = new Vectol((i * -1), i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 9] = new Vectol(i, i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 18] = new Vectol((i * -1), (i * -1) * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 27] = new Vectol(i, (i * -1) * -1);
        }
    }
    
    // html
    this.html = "角";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_kaku.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_kaku.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1500;
};

// ------------------------------
// 駒 飛車
// ------------------------------
// コンストラクタ
var Hisha = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(36);
    for (i = 0; i < 9; i++) {
        this.vectol[i] = new Vectol(0, i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 9] = new Vectol(i, 0);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 18] = new Vectol(0, (i * -1));
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 27] = new Vectol( (i * -1), 0);
    }
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        for (i = 0; i < 9; i++) {
            this.vectol[i] = new Vectol(0, i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 9] = new Vectol(i, 0 * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 18] = new Vectol(0, (i * -1) * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 27] = new Vectol((i * -1), 0 * -1);
        }
    }
    
    // html
    this.html = "飛";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_hisha.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_hisha.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 900;
    
    // 持ち駒点数
    this.scoreMotigoma = 2000;
};

// ------------------------------
// 駒 王
// ------------------------------
// コンストラクタ
var Ou = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(8);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    this.vectol[6] = new Vectol(1, -1);
    this.vectol[7] = new Vectol(-1, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
        this.vectol[6] = new Vectol(1, -1 * -1);
        this.vectol[7] = new Vectol(-1, -1 * -1);
    }
    
    // html
    this.html = "王";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_ou.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_ou.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 10000;
    
    // 持ち駒点数
    this.scoreMotigoma = 10000;
};

// ------------------------------
// 駒 馬
// ------------------------------
// コンストラクタ
var Uma = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(44);
    for (i = 0; i < 9; i++) {
        this.vectol[i] = new Vectol( (i * -1), i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 9] = new Vectol(i, i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 18] = new Vectol( (i * -1), (i * -1));
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 27] = new Vectol(i, (i * -1));
    }
    this.vectol[36] = new Vectol(-1, 1);
    this.vectol[37] = new Vectol(0, 1);
    this.vectol[38] = new Vectol(1, 1);
    this.vectol[39] = new Vectol(-1, 0);
    this.vectol[40] = new Vectol(1, 0);
    this.vectol[41] = new Vectol(0, -1);
    this.vectol[42] = new Vectol(1, -1);
    this.vectol[43] = new Vectol(-1, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        for (i = 0; i < 9; i++) {
            this.vectol[i] = new Vectol((i * -1), i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 9] = new Vectol(i, i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 18] = new Vectol((i * -1), (i * -1) * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 27] = new Vectol(i, (i * -1) * -1);
        }
        this.vectol[36] = new Vectol(-1, 1 * -1);
        this.vectol[37] = new Vectol(0, 1 * -1);
        this.vectol[38] = new Vectol(1, 1 * -1);
        this.vectol[39] = new Vectol(-1, 0 * -1);
        this.vectol[40] = new Vectol(1, 0 * -1);
        this.vectol[41] = new Vectol(0, -1 * -1);
        this.vectol[42] = new Vectol(1, -1 * -1);
        this.vectol[43] = new Vectol(-1, -1 * -1);
    }
    
    // html
    this.html = "馬";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_uma.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_uma.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 1000;
    
    // 持ち駒点数
    this.scoreMotigoma = 1500;
};

// ------------------------------
// 駒 竜
// ------------------------------
// コンストラクタ
var Ryu = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(44);
    for (i = 0; i < 9; i++) {
        this.vectol[i] = new Vectol(0, i);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 9] = new Vectol(i, 0);
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 18] = new Vectol(0, (i * -1));
    }
    for (i = 0; i < 9; i++) {
        this.vectol[i + 27] = new Vectol( (i * -1), 0);
    }
    this.vectol[36] = new Vectol(-1, 1);
    this.vectol[37] = new Vectol(0, 1);
    this.vectol[38] = new Vectol(1, 1);
    this.vectol[39] = new Vectol(-1, 0);
    this.vectol[40] = new Vectol(1, 0);
    this.vectol[41] = new Vectol(0, -1);
    this.vectol[42] = new Vectol(1, -1);
    this.vectol[43] = new Vectol(-1, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        for (i = 0; i < 9; i++) {
            this.vectol[i] = new Vectol(0, i * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 9] = new Vectol(i, 0 * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 18] = new Vectol(0, (i * -1) * -1);
        }
        for (i = 0; i < 9; i++) {
            this.vectol[i + 27] = new Vectol((i * -1), 0 * -1);
        }
        this.vectol[36] = new Vectol(-1, 1 * -1);
        this.vectol[37] = new Vectol(0, 1 * -1);
        this.vectol[38] = new Vectol(1, 1 * -1);
        this.vectol[39] = new Vectol(-1, 0 * -1);
        this.vectol[40] = new Vectol(1, 0 * -1);
        this.vectol[41] = new Vectol(0, -1 * -1);
        this.vectol[42] = new Vectol(1, -1 * -1);
        this.vectol[43] = new Vectol(-1, -1 * -1);
    }
    
    // html
    this.html = "竜";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_ryu.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_ryu.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 1100;
    
    // 持ち駒点数
    this.scoreMotigoma = 2000;
};

// ------------------------------
// 駒 と
// ------------------------------
// コンストラクタ
var To = function(teban){
    // 向き
    this.muki = teban;
    
    // ベクトル
    this.vectol = new Array(6);
    this.vectol[0] = new Vectol(-1, 1);
    this.vectol[1] = new Vectol(0, 1);
    this.vectol[2] = new Vectol(1, 1);
    this.vectol[3] = new Vectol(-1, 0);
    this.vectol[4] = new Vectol(1, 0);
    this.vectol[5] = new Vectol(0, -1);
    
    // 先手の場合はベクトルを上下逆にする
    if (this.muki === SENTE) {
        this.vectol[0] = new Vectol(-1, 1 * -1);
        this.vectol[1] = new Vectol(0, 1 * -1);
        this.vectol[2] = new Vectol(1, 1 * -1);
        this.vectol[3] = new Vectol(-1, 0 * -1);
        this.vectol[4] = new Vectol(1, 0 * -1);
        this.vectol[5] = new Vectol(0, -1 * -1);
    }
    
    // html
    this.html = "と";
    
    // imgsrc
    if (this.muki === SENTE) {
        this.imgsrc = FOLDER_IMG + "sente_to.png";
    } else {
        this.imgsrc = FOLDER_IMG + "gote_to.png";
    }
    
    // 盤上点数
    this.scoreBanzyou = 800;
    
    // 持ち駒点数
    this.scoreMotigoma = 1000;
};

// ------------------------------
// 駒 無し
// ------------------------------
// コンストラクタ
var Non = function(){
    // 向き
    this.muki = NONSTR;
    
    // ベクトル
    this.vectol = NONSTR;
    
    // html
    this.html = NONSTR;
    
    // imgsrc
    this.imgsrc = FOLDER_IMG + "non.png";
    
    // 盤上点数
    this.scoreBanzyou = 0;
    
    // 持ち駒点数
    this.scoreMotigoma = 0;
    
    // 堅さ点数
    this.scoreKatasa = 0;
    
    // 連結点数
    this.scoreRenketu = 0;
};