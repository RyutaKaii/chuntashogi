// ------------------------------
// 定石
// 定石を定義。
// 
// ------------------------------
// コンストラクタ
var Zyoseki = function(teban){
    // カウント設定 戦法
    this.countSenpou = 0;
    // カウント設定 囲い
    this.countKakoi = 0;
};

Zyoseki.prototype = {
    // 実行する(戦法)
    executeSenpou: function (tugiban, teban, senpou) {
        switch(senpou) {
            case SENPOU_BOUGIN:
                this.senpouArray = new Bougin(teban);
                break;
            case SENPOU_SIKENBISHA:
                this.senpouArray = new SikenHisya(teban);
                break;
            default:
                this.senpouArray = new Bougin(teban);
        }
        
        var humanAi = new HumanAi(teban);
        var tugibanClone = tugiban.clone();
        
        // 動かす駒が存在しない場合は除外
        if (this.countSenpou >= this.senpouArray.zyosekiArray.length) {
            return NONSTR;
        }
        var koma = this.senpouArray.zyosekiArray[this.countSenpou].koma;
        var fromVx = this.senpouArray.zyosekiArray[this.countSenpou].fromV.x;
        var fromVy = this.senpouArray.zyosekiArray[this.countSenpou].fromV.y;
        // 自分の駒かつ駒の種類が一致するか
        if (tugibanClone.field[fromVx][fromVy].html !== koma.html || tugibanClone.field[fromVx][fromVy].muki !== teban) {
            return NONSTR;
        }
        
        var result = humanAi.move(this.senpouArray.zyosekiArray[this.countSenpou].fromV, this.senpouArray.zyosekiArray[this.countSenpou].toV, this.senpouArray.zyosekiArray[this.countSenpou].koma, tugibanClone, true);
        
        if (!result) {
            return NONSTR;
        }
        
        // 初回のロード時のインクリメントを防ぐため
        if (isAiExecute) {
            this.countSenpou++;
        }
        
        return tugibanClone;
    },
    
    // 実行する(囲う)
    executeKakou: function (tugiban, teban, kakoi) {
        switch(kakoi) {
            case KAKOI_FUNAGAKOI:
                this.kakoiArray = new FunaGakoi(teban);
                break;
            case KAKOI_KANIGAKOI:
                this.kakoiArray = new KaniGakoi(teban);
                break;
            case KAKOI_MINOGAKOI:
                this.kakoiArray = new MinoGakoi(teban);
                break;
            default:
                this.kakoiArray = new KaniGakoi(teban);
        }
        
        var humanAi = new HumanAi(teban);
        var tugibanClone = tugiban.clone();
        
        // 動かす駒が存在しない場合は除外
        if (this.countKakoi >= this.kakoiArray.kakoiArray.length) {
            return NONSTR;
        }
        var koma = this.kakoiArray.kakoiArray[this.countKakoi].koma;
        var fromVx = this.kakoiArray.kakoiArray[this.countKakoi].fromV.x;
        var fromVy = this.kakoiArray.kakoiArray[this.countKakoi].fromV.y;
        // 自分の駒かつ駒の種類が一致するか
        if (tugibanClone.field[fromVx][fromVy].html !== koma.html || tugibanClone.field[fromVx][fromVy].muki !== teban) {
            return NONSTR;
        }
        
        var result = humanAi.move(this.kakoiArray.kakoiArray[this.countKakoi].fromV, this.kakoiArray.kakoiArray[this.countKakoi].toV, this.kakoiArray.kakoiArray[this.countKakoi].koma, tugibanClone, true);
        
        if (!result) {
            return NONSTR;
        }
        
        // 初回のロード時のインクリメントを防ぐため
        if (isAiExecute) {
            this.countKakoi++;
        }
        
        return tugibanClone;
    }
};

// ------------------------------
// 戦法 棒銀
// ------------------------------
// コンストラクタ
var Bougin = function(teban){
    this.zyosekiArray = new Array();
    this.zyosekiArray.push(new Itte(new Vectol(6, 2), new Vectol(6, 3), new Fu(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(1, 2), new Vectol(1, 3), new Fu(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(1, 3), new Vectol(1, 4), new Fu(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(2, 0), new Vectol(2, 1), new Gin(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(2, 1), new Vectol(1, 2), new Gin(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(1, 2), new Vectol(1, 3), new Gin(teban)));
};

// ------------------------------
// 戦法 四間飛車
// ------------------------------
// コンストラクタ
var SikenHisya = function(teban){
    this.zyosekiArray = new Array();
    this.zyosekiArray.push(new Itte(new Vectol(6, 2), new Vectol(6, 3), new Fu(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(5, 2), new Vectol(5, 3), new Fu(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(7, 1), new Vectol(6, 2), new Kaku(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(1, 1), new Vectol(5, 1), new Hisha(teban)));
    this.zyosekiArray.push(new Itte(new Vectol(6, 0), new Vectol(6, 1), new Gin(teban)));
};

// ------------------------------
// 囲い 船囲い
// ------------------------------
// コンストラクタ
var FunaGakoi = function(teban){
    this.kakoiArray = new Array();
    this.kakoiArray.push(new Itte(new Vectol(4, 0), new Vectol(5, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(5, 1), new Vectol(6, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(6, 0), new Vectol(5, 1), new Gin(teban)));
    this.kakoiArray.push(new Itte(new Vectol(4, 2), new Vectol(4, 3), new Fu(teban)));
    this.kakoiArray.push(new Itte(new Vectol(5, 1), new Vectol(4, 2), new Gin(teban)));
    this.kakoiArray.push(new Itte(new Vectol(3, 0), new Vectol(4, 1), new Kin(teban)));
};

// ------------------------------
// 囲い カニ囲い
// ------------------------------
// コンストラクタ
var KaniGakoi = function(teban){
    this.kakoiArray = new Array();
    this.kakoiArray.push(new Itte(new Vectol(4, 0), new Vectol(5, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(5, 0), new Vectol(6, 1), new Kin(teban)));
    this.kakoiArray.push(new Itte(new Vectol(3, 0), new Vectol(4, 1), new Kin(teban)));
};

// ------------------------------
// 囲い 美濃囲い
// ------------------------------
// コンストラクタ
var MinoGakoi = function(teban){
    this.kakoiArray = new Array();
    this.kakoiArray.push(new Itte(new Vectol(4, 0), new Vectol(3, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(3, 1), new Vectol(2, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(2, 1), new Vectol(1, 1), new Ou(teban)));
    this.kakoiArray.push(new Itte(new Vectol(2, 0), new Vectol(2, 1), new Gin(teban)));
    this.kakoiArray.push(new Itte(new Vectol(5, 0), new Vectol(4, 1), new Kin(teban)));
};

// ------------------------------
// 1手
// ------------------------------
// コンストラクタ
var Itte = function(fromV, toV, koma){
    this.fromV = fromV;
    this.toV = toV;
    this.koma = koma;
};